package tests;

import java.io.IOException;
import java.util.ArrayList;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import metier.CarteTimeline;
import metier.Jeu;
import metier.Joueur;

class VerifFinDePartieTest {

	@Before
	public Jeu initialisationJeu(int nb) throws IOException {
		Jeu jeu = new Jeu(new ArrayList<Joueur>());
		
		for(int i = 0; i < 10; i++) {
			CarteTimeline carte = new CarteTimeline(null, null, null, i, (double) i);
			jeu.getCartesPioche().add(carte);
		}
		
		
		for (int i = 0; i < 8; i++) {
			Joueur joueur = new Joueur("joueur" + i, null);
			jeu.getJoueurs().add(joueur);
		}
		for (int j = 0; j < nb; j++) {
			jeu.getJoueurs().get(j).setTermine(true);
		}
		jeu.verifFinPartie();
		return jeu;
	}

	@Test
	void testVerifFinUnJoueur() throws IOException {
		Jeu jeu = initialisationJeu(1);
		Assertions.assertThat(jeu.getJoueurs().size()).isEqualTo(1);
		Assertions.assertThat(jeu.getJoueursPerdants().size()).isEqualTo(7);
	}
	
	@Test
	void testVerifFinDeuxJoueur() throws IOException {
		Jeu jeu = initialisationJeu(2);
		Assertions.assertThat(jeu.getJoueurs().size()).isEqualTo(2);
		Assertions.assertThat(jeu.getJoueursPerdants().size()).isEqualTo(6);
		for(Joueur j : jeu.getJoueurs()) {
			Assertions.assertThat(j.getCartesJoueur().size()).isEqualTo(1);
		}
	}

}
