package vues;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

public class vueAjoutCarte extends JFrame {

	public vueAjoutCarte() {
		setTitle("Ajout de cartes");
		JPanel panel = new JPanel();
		JButton timeline = new JButton("Carte Timeline");
		JButton cardline = new JButton("Carte Cardline");

		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;
		panel.add(timeline, c);
		timeline.setFont(new Font("Mshtakan", Font.ITALIC, 20));
		timeline.setPreferredSize(new Dimension(150, 50));
		timeline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				JFrame time = new JFrame("Ajout carte timeline");
				JPanel panel = new JPanel();
				JPanel panel0 = new JPanel();
				JPanel panel2 = new JPanel();

				panel0.setLayout(new GridBagLayout());
				GridBagConstraints c1 = new GridBagConstraints();

				c1.gridx = 0;
				c1.gridy = 0;
				panel0.add(panel, c1);
				c1.gridy = 1;
				panel0.add(panel2, c1);

				panel.setBorder(BorderFactory.createTitledBorder(null, "Ajout Carte Timeline", TitledBorder.CENTER,
						TitledBorder.DEFAULT_POSITION, (new Font("Mshtakan", Font.BOLD, 13))));
				panel.setLayout(new GridBagLayout());
				GridBagConstraints c = new GridBagConstraints();

				JLabel inv = new JLabel("Invention: ");
				inv.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField invention = new JTextField();
				invention.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				invention.setPreferredSize(new Dimension(100, 25));
				c.gridx = 0;
				c.gridy = 0;
				panel.add(inv, c);
				c.gridx = 1;
				c.gridy = 0;
				panel.add(invention, c);

				JLabel d = new JLabel("Date: ");
				d.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField date = new JTextField();
				date.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				date.setPreferredSize(new Dimension(50, 25));
				c.gridx = 0;
				c.gridy = 1;
				panel.add(d, c);
				c.gridx = 1;
				c.gridy = 1;
				panel.add(date, c);

				JLabel nom = new JLabel("Nom de l'image: ");
				nom.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField nomImage = new JTextField();
				nomImage.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				nomImage.setPreferredSize(new Dimension(100, 25));
				c.gridx = 0;
				c.gridy = 2;
				panel.add(nom, c);
				c.gridx = 1;
				c.gridy = 2;
				panel.add(nomImage, c);

				JButton visible = new JButton("Chargement Image visible");
				visible.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JButton cache = new JButton("Chargement Image cachée");
				cache.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				c.gridx = 0;
				c.gridy = 3;
				panel.add(visible, c);
				c.gridx = 0;
				c.gridy = 4;
				panel.add(cache, c);

				visible.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						JFileChooser fileChooser = new JFileChooser();
						fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						FileNameExtensionFilter filtre = new FileNameExtensionFilter("jpg", "jpeg");
						fileChooser.addChoosableFileFilter(filtre);
						fileChooser.setFileFilter(filtre);
						fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
						int result = fileChooser.showOpenDialog(fileChooser);
						if (result == JFileChooser.APPROVE_OPTION && nomImage.getText().isEmpty() == false) {
							fileChooser.getSelectedFile()
									.renameTo(new File("data/timeline/cards/" + nomImage.getText() + "_date.jpeg"));
						} else {
							JOptionPane.showMessageDialog(null,
									"Veuillez remplir le champ nom de l'image avant de charger celle-ci !!",
									"Informations", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});

				cache.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						JFileChooser fileChooser = new JFileChooser();
						fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						FileNameExtensionFilter filtre = new FileNameExtensionFilter("jpg", "jpeg");
						fileChooser.addChoosableFileFilter(filtre);
						fileChooser.setFileFilter(filtre);
						fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
						int result = fileChooser.showOpenDialog(fileChooser);
						if (result == JFileChooser.APPROVE_OPTION && nomImage.getText().isEmpty() == false) {
							fileChooser.getSelectedFile()
									.renameTo(new File("data/timeline/cards/" + nomImage.getText() + ".jpeg"));
						} else {
							JOptionPane.showMessageDialog(null,
									"Veuillez remplir le champ nom de l'image avant de charger celle-ci !!",
									"Informations", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});

				time.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosing(WindowEvent windowEvent) {
						System.exit(0);
					}
				});

				JButton valider = new JButton("Valider");
				valider.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				valider.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (invention.getText().isEmpty() == true || nomImage.getText().isEmpty() == true
								|| date.getText().isEmpty() == true) {
							JOptionPane.showMessageDialog(null, "Veuillez remplir les champs texte !!", "Informations",
									JOptionPane.INFORMATION_MESSAGE);
						} else {
							File csvFile = new File("data/timeline/timeline.csv");
							PrintStream l_out = null;
							if (!csvFile.exists())
								try {
									throw new FileNotFoundException("Le fichier n'existe pas");
								} catch (FileNotFoundException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							else {
								try {
									l_out = new PrintStream(new FileOutputStream("data/timeline/timeline.csv", true));
								} catch (FileNotFoundException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}

							int suite;
							try {
								Integer.parseInt(date.getText());
								suite = 1;
							} catch (Exception exep) {
								JOptionPane.showMessageDialog(null,
										"Veuillez remplir le champ date avec un nombre entier (-556 ou 1978) !!",
										"Informations", JOptionPane.INFORMATION_MESSAGE);
								suite = 0;
							}

							if (suite == 1) {
								l_out.print(invention.getText() + ";");
								l_out.print(Integer.parseInt(date.getText()) + ";");
								l_out.print(nomImage.getText() + "\n");

								l_out.flush();
								l_out.close();
								l_out = null;

								time.dispose();
								VueLancement vue = new VueLancement();
							}
						}
					}
				});
				panel2.add(valider);

				time.add(panel0);
				time.setPreferredSize(new Dimension(500, 250));
				time.pack();
				time.setLocationRelativeTo(null);
				time.setVisible(true);
			}
		});

		c.gridx = 0;
		c.gridy = 1;
		panel.add(cardline, c);
		cardline.setFont(new Font("Mshtakan", Font.ITALIC, 20));
		cardline.setPreferredSize(new Dimension(150, 50));
		cardline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				JFrame time = new JFrame("Ajout carte cardline");
				JPanel panel = new JPanel();
				JPanel panel0 = new JPanel();
				JPanel panel2 = new JPanel();

				panel0.setLayout(new GridBagLayout());
				GridBagConstraints c1 = new GridBagConstraints();

				c1.gridx = 0;
				c1.gridy = 0;
				panel0.add(panel, c1);
				c1.gridy = 1;
				panel0.add(panel2, c1);

				panel.setBorder(BorderFactory.createTitledBorder(null, "Ajout Carte cardline", TitledBorder.CENTER,
						TitledBorder.DEFAULT_POSITION, (new Font("Mshtakan", Font.BOLD, 13))));
				panel.setLayout(new GridBagLayout());
				GridBagConstraints c = new GridBagConstraints();

				JLabel inv = new JLabel("Invention: ");
				inv.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField invention = new JTextField();
				invention.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				invention.setPreferredSize(new Dimension(150, 25));
				c.gridx = 0;
				c.gridy = 0;
				panel.add(inv, c);
				c.gridx = 1;
				c.gridy = 0;
				panel.add(invention, c);

				JLabel superf = new JLabel("Superficie: ");
				superf.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField superficie = new JTextField();
				superficie.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				superficie.setPreferredSize(new Dimension(80, 25));
				c.gridx = 0;
				c.gridy = 1;
				panel.add(superf, c);
				c.gridx = 1;
				c.gridy = 1;
				panel.add(superficie, c);

				JLabel pop = new JLabel("Population: ");
				pop.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField population = new JTextField();
				population.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				population.setPreferredSize(new Dimension(80, 25));
				c.gridx = 0;
				c.gridy = 2;
				panel.add(pop, c);
				c.gridx = 1;
				c.gridy = 2;
				panel.add(population, c);

				JLabel p = new JLabel("PIB: ");
				p.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField pib = new JTextField();
				pib.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				pib.setPreferredSize(new Dimension(80, 25));
				c.gridx = 0;
				c.gridy = 3;
				panel.add(p, c);
				c.gridx = 1;
				c.gridy = 3;
				panel.add(pib, c);

				JLabel pol = new JLabel("Pollution: ");
				pol.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField pollution = new JTextField();
				pollution.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				pollution.setPreferredSize(new Dimension(80, 25));
				c.gridx = 0;
				c.gridy = 4;
				panel.add(pol, c);
				c.gridx = 1;
				c.gridy = 4;
				panel.add(pollution, c);

				JLabel nom = new JLabel("Nom de l'image: ");
				nom.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JTextField nomImage = new JTextField();
				nomImage.setFont(new Font("Mshtakan", Font.ITALIC, 15));
				nomImage.setPreferredSize(new Dimension(100, 25));
				c.gridx = 0;
				c.gridy = 5;
				panel.add(nom, c);
				c.gridx = 1;
				c.gridy = 5;
				panel.add(nomImage, c);

				JButton visible = new JButton("Chargement Image visible");
				visible.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				JButton cache = new JButton("Chargement Image cachée");
				cache.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				c.gridx = 0;
				c.gridy = 6;
				panel.add(visible, c);
				c.gridx = 0;
				c.gridy = 7;
				panel.add(cache, c);

				visible.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						JFileChooser fileChooser = new JFileChooser();
						fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						FileNameExtensionFilter filtre = new FileNameExtensionFilter("jpg", "jpeg");
						fileChooser.addChoosableFileFilter(filtre);
						fileChooser.setFileFilter(filtre);
						fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
						int result = fileChooser.showOpenDialog(fileChooser);
						if (result == JFileChooser.APPROVE_OPTION && nomImage.getText().isEmpty() == false) {
							fileChooser.getSelectedFile()
									.renameTo(new File("data/cardline/cards/" + nomImage.getText() + "_reponse.jpeg"));
						} else {
							JOptionPane.showMessageDialog(null,
									"Veuillez remplir le champ nom de l'image avant de charger celle-ci !!",
									"Informations", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});

				cache.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						JFileChooser fileChooser = new JFileChooser();
						fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						FileNameExtensionFilter filtre = new FileNameExtensionFilter("jpg", "jpeg");
						fileChooser.addChoosableFileFilter(filtre);
						fileChooser.setFileFilter(filtre);
						fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
						int result = fileChooser.showOpenDialog(fileChooser);
						if (result == JFileChooser.APPROVE_OPTION && nomImage.getText().isEmpty() == false) {
							fileChooser.getSelectedFile()
									.renameTo(new File("data/cardline/cards/" + nomImage.getText() + ".jpeg"));
						} else {
							JOptionPane.showMessageDialog(null,
									"Veuillez remplir le champ nom de l'image avant de charger celle-ci !!",
									"Informations", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});

				time.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosing(WindowEvent windowEvent) {
						System.exit(0);
					}
				});

				JButton valider = new JButton("Valider");
				valider.setFont(new Font("Mshtakan", Font.ITALIC, 20));
				valider.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (invention.getText().isEmpty() == true || nomImage.getText().isEmpty() == true
								|| superficie.getText().isEmpty() == true || population.getText().isEmpty() == true
								|| pib.getText().isEmpty() == true || pollution.getText().isEmpty() == true) {
							JOptionPane.showMessageDialog(null, "Veuillez remplir les champs texte !!", "Informations",
									JOptionPane.INFORMATION_MESSAGE);
						} else {
							File csvFile = new File("data/cardline/cardline.csv");
							PrintStream l_out = null;
							if (!csvFile.exists())
								try {
									throw new FileNotFoundException("Le fichier n'existe pas");
								} catch (FileNotFoundException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							else {
								try {
									l_out = new PrintStream(new FileOutputStream("data/cardline/cardline.csv", true));
								} catch (FileNotFoundException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}

							int suite1;
							try {
								Double.parseDouble(superficie.getText().replaceAll(",", "."));
								suite1 = 1;
							} catch (Exception exep) {
								JOptionPane.showMessageDialog(null,
										"Veuillez remplir le champ superficie avec un nombre entier ou réel !!", "Informations",
										JOptionPane.INFORMATION_MESSAGE);
								suite1 = 0;
							}
							
							int suite2;
							try {
								Integer.parseInt(population.getText());
								suite2 = 1;
							} catch (Exception exep) {
								JOptionPane.showMessageDialog(null,
										"Veuillez remplir le champ population avec un nombre entier !!", "Informations",
										JOptionPane.INFORMATION_MESSAGE);
								suite2 = 0;
							}
							
							int suite3;
							try {
								Integer.parseInt(pib.getText());
								suite3 = 1;
							} catch (Exception exep) {
								JOptionPane.showMessageDialog(null,
										"Veuillez remplir le champ pib avec un nombre entier !!", "Informations",
										JOptionPane.INFORMATION_MESSAGE);
								suite3 = 0;
							}
							
							int suite4;
							try {
								Double.parseDouble(pollution.getText().replaceAll(",", "."));
								suite4 = 1;
							} catch (Exception exep) {
								JOptionPane.showMessageDialog(null,
										"Veuillez remplir le champ pollution avec un nombre entier ou réel !!", "Informations",
										JOptionPane.INFORMATION_MESSAGE);
								suite4 = 0;
							}

							if (suite1 == 1 && suite2 == 1 && suite3 == 1 && suite4 == 1) {
								l_out.print(invention.getText() + ";");
								l_out.print(superficie.getText().replaceAll(".", ",") + ";");
								l_out.print(population.getText() + ";");
								l_out.print(pib.getText() + ";");
								l_out.print(pollution.getText().replaceAll(".", ",") + ";");
								l_out.print(nomImage.getText() + "\n");

								l_out.flush();
								l_out.close();
								l_out = null;

								time.dispose();
								VueLancement vue = new VueLancement();
							}
						}
					}
				});
				panel2.add(valider);

				time.add(panel0);
				time.setPreferredSize(new Dimension(550, 350));
				time.pack();
				time.setLocationRelativeTo(null);
				time.setVisible(true);
			}
		});

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});

		JButton retour = new JButton("Retour");
		c.gridx = 0;
		c.gridy = 3;
		panel.add(retour, c);
		retour.setFont(new Font("Mshtakan", Font.ITALIC, 20));
		retour.setPreferredSize(new Dimension(150, 50));
		retour.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				VueLancement vue = new VueLancement();
			}
		});

		add(panel);
		setPreferredSize(new Dimension(250, 200));
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
}