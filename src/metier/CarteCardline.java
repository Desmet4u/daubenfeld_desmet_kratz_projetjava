package metier;

public class CarteCardline extends Carte {
	/**
	 * attributs de la classe CarteCardline qui determinent le theme de la carte : superficie, population, pib, pollution
	 */
	private double supericie;
	private int population;
	private int pib;
	private double pollution;

	/**
	 * Constructeur de l'objet CarteCardline avec attributs
	 * @param invention
	 * @param imageCachee
	 * @param imageVisible
	 * @param supericie
	 * @param population
	 * @param pib
	 * @param pollution
	 * @param valeur
	 */
	public CarteCardline(String invention, String imageCachee, String imageVisible, double supericie, int population,
			int pib, double pollution, Double valeur) {
		super(invention, imageCachee, imageVisible, valeur);
		this.supericie = supericie;
		this.population = population;
		this.pib = pib;
		this.pollution = pollution;
	}

	/**
	 * methodes retournant ou modifiant les attributs de la classe Carte
	 * @return
	 */
	public double getSupericie() {
		return supericie;
	}

	public void setSupericie(double supericie) {
		this.supericie = supericie;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public int getPib() {
		return pib;
	}

	public void setPib(int pib) {
		this.pib = pib;
	}

	public double getPollution() {
		return pollution;
	}

	public void setPollution(double pollution) {
		this.pollution = pollution;
	}

}
