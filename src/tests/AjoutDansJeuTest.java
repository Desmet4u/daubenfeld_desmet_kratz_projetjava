package tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import metier.CarteTimeline;
import metier.Jeu;
import metier.Joueur;

class AjoutDansJeuTest {

	@Before
	public Jeu initialisationJeu(CarteTimeline carte, int gaucheDroite) throws IOException {
		Jeu jeu = new Jeu(new ArrayList<Joueur>());
		Joueur j = new Joueur("test", new Date());
		j.getCartesJoueur().add(carte);
		CarteTimeline c = new CarteTimeline(null, null, null, 5, (double) 5);
		jeu.getJoueurs().add(j);
		jeu.getCartesJeu().add(c);
		jeu.ajoutDansJeu(j, c, carte, gaucheDroite);
		return jeu;
	}

	@Test
	void testAjoutCarteGauche() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 3, (double) 3);
		Jeu jeu = initialisationJeu(c,0);
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getJoueurs().get(0));
	}
	
	@Test
	void testAjoutCarteDroite() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 7, (double) 3);
		Jeu jeu = initialisationJeu(c,1);
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getJoueurs().get(0));
	}

}
