package metier;

import java.io.Serializable;

public abstract class Carte implements Serializable{
	/**
	 * attributs de la classe abstraite Carte : libelle, image cach�e, image visible et valeur
	 */
	private String libelle;
	private String imageCachee;
	private String imageVisible;
	private Double valeur;

	/**
	 * Constructeur de Carte
	 * @param libelle
	 * @param imageCachee
	 * @param imageVisible
	 * @param valeur
	 */
	public Carte(String libelle, String imageCachee, String imageVisible, Double valeur) {
		super();
		this.libelle = libelle;
		this.imageCachee = imageCachee;
		this.imageVisible = imageVisible;
		this.valeur = valeur;
	}

	/**
	 * methodes retournant ou modifiant les attributs de la classe Carte
	 * @return
	 */
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getImageCachee() {
		return imageCachee;
	}

	public void setImageCachee(String imageCachee) {
		this.imageCachee = imageCachee;
	}

	public String getImageVisible() {
		return imageVisible;
	}

	public void setImageVisible(String imageVisible) {
		this.imageVisible = imageVisible;
	}

	public Double getValeur() {
		return valeur;
	}

	public void setValeur(Double valeur) {
		this.valeur = valeur;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imageCachee == null) ? 0 : imageCachee.hashCode());
		result = prime * result + ((imageVisible == null) ? 0 : imageVisible.hashCode());
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + ((valeur == null) ? 0 : valeur.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carte other = (Carte) obj;
		if (imageCachee == null) {
			if (other.imageCachee != null)
				return false;
		} else if (!imageCachee.equals(other.imageCachee))
			return false;
		if (imageVisible == null) {
			if (other.imageVisible != null)
				return false;
		} else if (!imageVisible.equals(other.imageVisible))
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (valeur == null) {
			if (other.valeur != null)
				return false;
		} else if (!valeur.equals(other.valeur))
			return false;
		return true;
	}
}
