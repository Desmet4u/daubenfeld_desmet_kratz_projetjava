package vues;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import enums.ThemeCarte;
import enums.TypeCarte;
import metier.Jeu;
import metier.Joueur;

public class InterfaceChoixTheme extends JFrame {

	public InterfaceChoixTheme(ArrayList<Joueur> arraylistJoueurs) {

		JPanel cardLinePanel = new JPanel();
		JLabel cardLineLabel = new JLabel("Avec quel thème souhaitez-vous jouer ?");
		JButton jouer = new JButton("Jouer");
		cardLinePanel.setLayout(new GridLayout(6,1));
		
		CheckboxGroup cb = new CheckboxGroup();
		Checkbox superficie = new Checkbox("Superficie", cb, false);
		Checkbox pop = new Checkbox("Population", cb, false);
		Checkbox pib = new Checkbox("PIB", cb, false);
		Checkbox pollution = new Checkbox("Pollution", cb, false);

		jouer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (superficie.getState() || pop.getState() || pib.getState() || pollution.getState()) {
					Jeu test = new Jeu();
					test.getJoueurs().addAll(arraylistJoueurs);
					test.setTypeJeu(TypeCarte.cardline);

					if (superficie.getState())
						test.setThemeJeu(ThemeCarte.superficie);
					else if (pop.getState())
						test.setThemeJeu(ThemeCarte.population);
					else if (pib.getState())
						test.setThemeJeu(ThemeCarte.pib);
					else
						test.setThemeJeu(ThemeCarte.pollution);
					
					try {
						VueJeu jeu = new VueJeu(test);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});
		
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(getParent(), "Êtes-vous sur de vouloir quitter le jeu ?",
						"Fermeture du jeu ?", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});

		cardLinePanel.add(cardLineLabel);
		cardLinePanel.add(superficie);
		cardLinePanel.add(pop);
		cardLinePanel.add(pib);
		cardLinePanel.add(pollution);
		cardLinePanel.add(jouer);

		add(cardLinePanel);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);

	}

}
