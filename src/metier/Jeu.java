package metier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.StringTokenizer;

import enums.ThemeCarte;
import enums.TypeCarte;

public class Jeu implements Serializable {

	/**
	 * attributs de la classe Jeu : cartesJeu, cartesPioche, cartesDeFausse,
	 * joueurs, joueursPerdants, nomJeu, carteEnJeu, typeJeu, themeJeu, posJoueur
	 */
	private ArrayList<Carte> cartesJeu;
	private ArrayList<Carte> cartesPioche;
	private ArrayList<Carte> cartesDefausse;
	private ArrayList<Joueur> joueurs;
	private ArrayList<Joueur> joueursPerdants;
	private String nomJeu;
	private Carte carteEnJeu;
	private TypeCarte typeJeu;
	private ThemeCarte themeJeu;
	private int posJoueur; // utile pour vueJeu lors de la sauvegarde et chargement d'une partie
	private int tempsPartie;
	private boolean fini;

	/**
	 * Constructeur du Jeu avec tous les param�tres
	 * 
	 * @param cartesJeu
	 * @param cartesPioche
	 * @param cartesDefausse
	 * @param joueurs
	 * @param joueursPerdants
	 * @param nb
	 */
	public Jeu(ArrayList<Carte> cartesJeu, ArrayList<Carte> cartesPioche, ArrayList<Carte> cartesDefausse,
			ArrayList<Joueur> joueurs, ArrayList<Joueur> joueursPerdants) {
		super();
		this.cartesJeu = cartesJeu;
		this.cartesPioche = cartesPioche;
		this.cartesDefausse = cartesDefausse;
		this.joueurs = joueurs;
		this.joueursPerdants = joueursPerdants;
		this.tempsPartie = 0;
		this.fini = false;
	}

	public Jeu() {
		super();
		this.cartesJeu = new ArrayList<Carte>();
		this.cartesPioche = new ArrayList<Carte>();
		this.cartesDefausse = new ArrayList<Carte>();
		this.joueurs = new ArrayList<Joueur>();
		this.joueursPerdants = new ArrayList<Joueur>();
		this.posJoueur = 0;
		this.tempsPartie = 0;
		this.fini = false;
	}

	/**
	 * Constructeur de jeu avec deux parametres
	 * 
	 * @param joueurs
	 */
	public Jeu(ArrayList<Joueur> joueurs) {
		super();
		this.cartesJeu = new ArrayList<Carte>();
		this.cartesPioche = new ArrayList<Carte>();
		this.cartesDefausse = new ArrayList<Carte>();
		this.joueurs = joueurs;
		this.joueursPerdants = new ArrayList<Joueur>();
		this.tempsPartie = 0;
		this.fini = false;
	}

	public Jeu(Jeu readObject) {
		// TODO Auto-generated constructor stub
		this.carteEnJeu = readObject.carteEnJeu;
		this.cartesDefausse = readObject.cartesDefausse;
		this.cartesJeu = readObject.cartesJeu;
		this.cartesPioche = readObject.cartesPioche;
		this.joueurs = readObject.joueurs;
		this.joueursPerdants = readObject.joueursPerdants;
		this.nomJeu = readObject.nomJeu;
		this.themeJeu = readObject.themeJeu;
		this.typeJeu = readObject.typeJeu;
		this.posJoueur = readObject.posJoueur;
		this.tempsPartie = readObject.tempsPartie;
		this.fini = readObject.fini;
	}

	/**
	 * m�thode qui trie la liste de joueur du Jeu par date de naissance
	 */
	public void trieJoueur() {
		Collections.sort(this.joueurs,
				(joueur1, joueur2) -> joueur2.getDateDeNaissance().compareTo(joueur1.getDateDeNaissance()));
	}

	/**
	 * M�thode chargeant la carte du jeu en fonction du type et du th�me de la carte
	 * 
	 * @param type
	 * @param theme
	 * @throws IOException
	 */
	public void chargerCarte(TypeCarte type, ThemeCarte theme) throws IOException {

		// nomJeu=type.toString();

		if (type.equals(TypeCarte.timeline)) {
			this.setTypeJeu(TypeCarte.timeline);
			this.setThemeJeu(null);
			File file = new File("Data/timeline/timeline.csv");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			StringTokenizer st = null;
			String line = br.readLine();
			for (line = br.readLine(); line != null; line = br.readLine()) {
				st = new StringTokenizer(line, ";");
				String libelle = st.nextToken();
				int date = Integer.parseInt(st.nextToken());
				String imageNom = st.nextToken();

				String url = imageNom + ".jpeg";
				String url2 = imageNom + "_date.jpeg";

				CarteTimeline carte = new CarteTimeline(libelle, url, url2, date, (double) date);
				this.cartesPioche.add(carte);

			}
			br.close();
		} else {
			this.setTypeJeu(TypeCarte.cardline);
			File file = new File("Data/cardline/cardline.csv");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			StringTokenizer st = null;
			String line = br.readLine();
			for (line = br.readLine(); line != null; line = br.readLine()) {
				st = new StringTokenizer(line, ";");
				String libelle = st.nextToken();
				double superficie = Double.parseDouble(st.nextToken());
				int population = Integer.parseInt(st.nextToken());
				int pib = Integer.parseInt(st.nextToken());
				double pollution = Double.parseDouble(st.nextToken().replace(',', '.'));
				String imageNom = st.nextToken();

				String url = imageNom + ".jpeg";
				String url2 = imageNom + "_reponse.jpeg";

				Double valeur;
				switch (theme) {
				case superficie:
					valeur = superficie;
					this.setThemeJeu(ThemeCarte.superficie);
					break;

				case population:
					valeur = (double) population;
					this.setThemeJeu(ThemeCarte.population);
					break;

				case pib:
					valeur = (double) pib;
					this.setThemeJeu(ThemeCarte.pib);
					break;

				default:
					valeur = pollution;
					this.setThemeJeu(ThemeCarte.pollution);
					break;
				}
				CarteCardline carte = new CarteCardline(libelle, url, url2, superficie, population, pib, pollution,
						valeur);
				this.cartesPioche.add(carte);

			}
			br.close();
		}
		Collections.shuffle(cartesPioche);
	}

	public String toStringCartesPioche() {
		String res = "";
		for (Carte c : cartesPioche)
			res += c.getLibelle() + "\n";
		return res;
	}

	/**
	 * M�thode distribuant les cartes en fonction du nombre de joueur et en fonction
	 * des r�gles fix�es.
	 */
	public void distribuerCarte() {
		int nbCartes;
		switch (this.getJoueurs().size()) {
		case 2:
		case 3:
			nbCartes = 6;
			break;
		case 4:
		case 5:
			nbCartes = 5;
			break;

		default:
			nbCartes = 4;
			break;
		}

		for (int j = 0; j < nbCartes; j++) {
			for (int i = 0; i < this.getJoueurs().size(); i++) {
				Carte c = this.cartesPioche.get((int) (Math.random() * (this.cartesPioche.size())));
				this.joueurs.get(i).getCartesJoueur().add(c);
				this.cartesPioche.remove(c);
			}
		}

		Carte c = this.cartesPioche.get((int) (Math.random() * (this.cartesPioche.size())));
		this.cartesJeu.add(c);
		this.cartesPioche.remove(c);
	}

	/**
	 * Ajoute la carte dans le jeu en v�rifiant si elle est bien plac�e
	 * 
	 * @param joueur
	 * @param carte
	 * @param carteAPlacer
	 * @param gaucheDroite
	 */
	public void ajoutDansJeu(Joueur joueur, Carte carte, Carte carteAPlacer, int gaucheDroite) {
		int posCarte = this.getCartesJeu().indexOf(carte);
		if (gaucheDroite == 0) { // gauche
			this.getCartesJeu().add(posCarte, carteAPlacer);
		} else {
			this.getCartesJeu().add(posCarte + 1, carteAPlacer);
		}
		joueur.getCartesJoueur().remove(carteAPlacer);
		// verifPlacementTest(joueur, carteAPlacer);
	}

	/**
	 * Ajoute la carte dans le jeu à partir d'une position et vérifié si elle est
	 * bien placée dans le jeu
	 * 
	 * @param joueur
	 * @param pos
	 * @param carteAPlacer
	 */
	public void ajoutDansJeuTest(Joueur joueur, int pos, Carte carteAPlacer) {
		if (this.cartesJeu.size() == pos) {
			this.getCartesJeu().add(carteAPlacer);
		} else {
			this.getCartesJeu().add(pos, carteAPlacer);
		}
		joueur.getCartesJoueur().remove(carteAPlacer);
		verifPlacementTest(joueur, carteAPlacer);
	}

	/**
	 * Vérifie si la carte posée est bien placée et si pas bonne place alors
	 * distribue une nouvelle carte au joueur
	 * 
	 * @param joueur
	 * @param carteAPlacer
	 */
	public void verifPlacementTest(Joueur joueur, Carte carteAPlacer) {
		boolean trie = true;

		// verif carte de gauche et carte de droite de la carte a placer
		int posCarte = this.getCartesJeu().indexOf(carteAPlacer);
		if (posCarte == 0) {
			if (carteAPlacer.getValeur() > this.getCartesJeu().get(1).getValeur()) {
				trie = false;
			}
		} else if (posCarte == this.getCartesJeu().size() - 1) {
			if (carteAPlacer.getValeur() < this.getCartesJeu().get(posCarte - 1).getValeur()) {
				trie = false;
			}
		} else {
			if (carteAPlacer.getValeur() > this.getCartesJeu().get(posCarte + 1).getValeur()
					|| carteAPlacer.getValeur() < this.getCartesJeu().get(posCarte - 1).getValeur()) {
				trie = false;
			}
		}

		if (trie == false) {
			joueur.setTermine(false);
			this.getCartesDefausse().add(carteAPlacer);
			this.getCartesJeu().remove(carteAPlacer);
			if (this.getCartesPioche().size() == 0) {
				this.setCartesPioche(this.cartesDefausse);
				this.cartesDefausse = new ArrayList<Carte>();
			}
			Carte c = this.getCartesPioche().get(this.cartesPioche.size() - 1);
			joueur.getCartesJoueur().add(c);
			this.getCartesPioche().remove(c);
		} else {
			if (joueur.getCartesJoueur().size() == 0) {
				joueur.setTermine(true);
			}
		}
	}

	/**
	 * Vérifie qu'un ou plusieurs joueur(s) a(ont) fini et si la partie est ainsi
	 * finie
	 * 
	 * @return joueur s'il a gagn�
	 */
	public Joueur verifFinPartie() {
		ArrayList<Joueur> joueursGagnants = new ArrayList<Joueur>();
		for (Joueur joueur : this.getJoueurs()) {
			if (joueur.isTermine()) {
				joueursGagnants.add(joueur);
			}
		}
		
		if (joueursGagnants.size() == 1) {
			this.getJoueursPerdants().addAll(this.getJoueurs());
			this.getJoueursPerdants().removeAll(joueursGagnants);
			this.setJoueurs(joueursGagnants);
			return this.getJoueurs().get(0); // gagnant
		} else if (joueursGagnants.size() > 1) { // chaque joueur ayant fini rejoue avec une carte
			this.getJoueursPerdants().addAll(this.getJoueurs());
			this.getJoueursPerdants().removeAll(joueursGagnants);
			this.setJoueurs(joueursGagnants);

			for (Joueur joueur : this.getJoueurs()) {
				Carte carte = this.getCartesPioche().get((int) (Math.random() * (this.cartesPioche.size())));
				joueur.getCartesJoueur().add(carte);
				this.getCartesPioche().remove(carte);
				joueur.setTermine(false);
			}

		}
		return null;
	}

	/**
	 * Indique la bonne position d'une carte pour un bot et l'ajoute dans le jeu
	 * avec une proba de se tromper en fonction du niveau de difficulté
	 * 
	 * @param joueur
	 * @param carte
	 */
	public void choixPlacementCarteBot(Joueur joueur, Carte carte) {
		Integer bonnePos = null;
		int i = 0;
		while (bonnePos == null) {
			if (this.getCartesJeu().get(i).getValeur() > carte.getValeur()) {
				bonnePos = i;
			} else if (this.getCartesJeu().size() - 1 == i) {
				bonnePos = i + 1;
			} else {
				i++;
			}
		}

		int a, b;
		switch (joueur.getDifficulte()) {
		case debutant:
			a = (int) (Math.random() * 5);
			b = (int) (Math.random() * 5);

			if (a - b != 0) { // si 2 nombres différents (80% de chance que cela ce produise), on place à une
								// position au hasard
				bonnePos = (int) (Math.random() * (this.cartesJeu.size() + 1));
			}

			break;

		case cultive:
			a = (int) (Math.random() * 2);
			b = (int) (Math.random() * 2);

			if (a - b != 0) { // si 2 nombres différents (50% de chance que cela ce produise), on place à une
								// position au hasard
				bonnePos = (int) (Math.random() * (this.cartesJeu.size() + 1));
			}

			break;

		default:
			a = (int) (Math.random() * 5);
			b = (int) (Math.random() * 5);

			if (a - b == 0) { // si 2 nombres égaux (20% de chance que cela ce produise), on place à une
								// position au hasard
				bonnePos = (int) (Math.random() * (this.cartesJeu.size() + 1));
			}

			break;
		}

		ajoutDansJeuTest(joueur, bonnePos, carte);
	}

	/**
	 * Permet le changement de theme d'une carte et de réordonner le jeu en place
	 * 
	 * @param theme
	 */
	public void changementTheme(ThemeCarte theme) {
		for (Joueur joueur : this.getJoueurs()) {
			for (Carte c : joueur.getCartesJoueur()) {
				CarteCardline carte = (CarteCardline) c;
				switch (theme) {
				case superficie:
					c.setValeur(carte.getSupericie());
					break;

				case population:
					c.setValeur((double) carte.getPopulation());
					break;

				case pib:
					c.setValeur((double) carte.getPib());
					break;

				default:
					c.setValeur(carte.getPollution());
					break;
				}
			}
		}

		for (Carte c : this.getCartesJeu()) {
			CarteCardline carte = (CarteCardline) c;
			switch (theme) {
			case superficie:
				c.setValeur(carte.getSupericie());
				break;

			case population:
				c.setValeur((double) carte.getPopulation());
				break;

			case pib:
				c.setValeur((double) carte.getPib());
				break;

			default:
				c.setValeur(carte.getPollution());
				break;
			}
		}

		for (Carte c : this.getCartesPioche()) {
			CarteCardline carte = (CarteCardline) c;
			switch (theme) {
			case superficie:
				c.setValeur(carte.getSupericie());
				break;

			case population:
				c.setValeur((double) carte.getPopulation());
				break;

			case pib:
				c.setValeur((double) carte.getPib());
				break;

			default:
				c.setValeur(carte.getPollution());
				break;
			}
		}

		for (Carte c : this.getCartesDefausse()) {
			CarteCardline carte = (CarteCardline) c;
			switch (theme) {
			case superficie:
				c.setValeur(carte.getSupericie());
				break;

			case population:
				c.setValeur((double) carte.getPopulation());
				break;

			case pib:
				c.setValeur((double) carte.getPib());
				break;

			default:
				c.setValeur(carte.getPollution());
				break;
			}
		}
		this.setThemeJeu(theme);
		Collections.sort(this.cartesJeu,
				(carte1, carte2) -> carte1.getValeur().compareTo(carte2.getValeur()));
		
	}

	/**
	 * m�thodes retournant ou changeant la valeur de tous les attributs de la classe
	 * jeu
	 * 
	 * @return attributs de la classe Jeu
	 */
	public ArrayList<Carte> getCartesJeu() {
		return cartesJeu;
	}

	public void setCartesJeu(ArrayList<Carte> cartesJeu) {
		this.cartesJeu = cartesJeu;
	}

	public ArrayList<Carte> getCartesPioche() {
		return cartesPioche;
	}

	public void setCartesPioche(ArrayList<Carte> cartesPioche) {
		this.cartesPioche = cartesPioche;
	}

	public ArrayList<Carte> getCartesDefausse() {
		return cartesDefausse;
	}

	public void setCartesDefausse(ArrayList<Carte> cartesDefausse) {
		this.cartesDefausse = cartesDefausse;
	}

	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(ArrayList<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public ArrayList<Joueur> getJoueursPerdants() {
		return joueursPerdants;
	}

	public void setJoueursPerdants(ArrayList<Joueur> joueursPerdants) {
		this.joueursPerdants = joueursPerdants;
	}

	public String getNomJeu() {
		return nomJeu;
	}

	public void setNomJeu(String nomJeu) {
		this.nomJeu = nomJeu;
	}

	public Carte getCarteEnJeu() {
		return carteEnJeu;
	}

	public void setCarteEnJeu(Carte carteEnJeu) {
		this.carteEnJeu = carteEnJeu;
	}

	public TypeCarte getTypeJeu() {
		return typeJeu;
	}

	public void setTypeJeu(TypeCarte typeJeu) {
		this.typeJeu = typeJeu;
	}

	public ThemeCarte getThemeJeu() {
		return themeJeu;
	}

	public void setThemeJeu(ThemeCarte themeJeu) {
		this.themeJeu = themeJeu;
	}

	public int getPosJoueur() {
		return posJoueur;
	}

	public void setPosJoueur(int posJoueur) {
		this.posJoueur = posJoueur;
	}

	public int getTempsPartie() {
		return tempsPartie;
	}

	public void setTempsPartie(int tempsPartie) {
		this.tempsPartie = tempsPartie;
	}

	public boolean isFini() {
		return fini;
	}

	public void setFini(boolean fini) {
		this.fini = fini;
	}

}
