package tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import metier.CarteTimeline;
import metier.Jeu;
import metier.Joueur;

class VerifPlacementTest {

	@Before
	public Jeu initialisationJeu(int pos,CarteTimeline carte, int gaucheDroite) throws IOException {
		Jeu jeu = new Jeu(new ArrayList<Joueur>());
		Joueur j = new Joueur("test", new Date());
		j.getCartesJoueur().add(carte);
		CarteTimeline c = new CarteTimeline(null, null, null, 5, (double) 5);
		CarteTimeline c1 = new CarteTimeline(null, null, null, 7, (double) 7);
		
		CarteTimeline pioche = new CarteTimeline(null, null, null, 50, (double) 50);
		jeu.getCartesPioche().add(pioche);
		
		jeu.getJoueurs().add(j);
		jeu.getCartesJeu().add(c);
		jeu.getCartesJeu().add(c1);
		jeu.ajoutDansJeu(j, jeu.getCartesJeu().get(pos), carte, gaucheDroite);
		jeu.verifPlacementTest(j, carte);
		return jeu;
	}
	
	@Test
	void testJoueurPlusDeCarte() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 3, (double) 3);
		Jeu jeu = initialisationJeu(0,c,0);
		Assertions.assertThat(jeu.getJoueurs().get(0).getCartesJoueur().size()).isEqualTo(0);
		Assertions.assertThat(jeu.getJoueurs().get(0).isTermine()).isEqualTo(true);
		}

	@Test
	void testVerifPlacementGauche() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 3, (double) 3);
		Jeu jeu = initialisationJeu(0,c,0);
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getJoueurs().get(0));
		int pos = jeu.getCartesJeu().indexOf(c);
		Assertions.assertThat(jeu.getCartesJeu().get(pos).getValeur()).isLessThan(jeu.getCartesJeu().get(pos+1).getValeur());
	}
	
	@Test
	void testVerifPlacementDroite() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 10, (double) 10);
		Jeu jeu = initialisationJeu(1,c,1);
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getJoueurs().get(0));
		int pos = jeu.getCartesJeu().indexOf(c);
		Assertions.assertThat(jeu.getCartesJeu().get(pos).getValeur()).isGreaterThan(jeu.getCartesJeu().get(pos-1).getValeur());
	}
	
	@Test
	void testVerifPlacementEntre() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 6, (double) 6);
		Jeu jeu = initialisationJeu(1,c,0);
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getJoueurs().get(0));
		int pos = jeu.getCartesJeu().indexOf(c);
		Assertions.assertThat(jeu.getCartesJeu().get(pos).getValeur()).isLessThan(jeu.getCartesJeu().get(pos+1).getValeur());
		Assertions.assertThat(jeu.getCartesJeu().get(pos).getValeur()).isGreaterThan(jeu.getCartesJeu().get(pos-1).getValeur());
	}
	
	
	@Test
	void testVerifPlacementGaucheFausse() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 10, (double) 10);
		Jeu jeu = initialisationJeu(0,c,0);
		CarteTimeline pioche = new CarteTimeline(null, null, null, 50, (double) 50);
		Assertions.assertThat(c).isNotIn(jeu.getCartesJeu());
		Assertions.assertThat(jeu.getJoueurs().get(0).getCartesJoueur().get(0).getValeur()).isEqualTo(pioche.getValeur());
	}
	
	@Test
	void testVerifPlacementDroiteFausse() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 3, (double) 3);
		Jeu jeu = initialisationJeu(1,c,1);
		CarteTimeline pioche = new CarteTimeline(null, null, null, 50, (double) 50);
		Assertions.assertThat(c).isNotIn(jeu.getCartesJeu());
		Assertions.assertThat(jeu.getJoueurs().get(0).getCartesJoueur().get(0).getValeur()).isEqualTo(pioche.getValeur());
	}
	
	@Test
	void testVerifPlacementEntreFausse() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 10, (double) 10);
		Jeu jeu = initialisationJeu(1,c,0);
		CarteTimeline pioche = new CarteTimeline(null, null, null, 50, (double) 50);
		Assertions.assertThat(c).isNotIn(jeu.getCartesJeu());
		Assertions.assertThat(jeu.getJoueurs().get(0).getCartesJoueur().get(0).getValeur()).isEqualTo(pioche.getValeur());
	}
	
	@Test
	void testVerifPlacementEntre2Fausse() throws IOException {
		CarteTimeline c = new CarteTimeline(null, null, null, 4, (double) 4);
		Jeu jeu = initialisationJeu(1,c,0);
		CarteTimeline pioche = new CarteTimeline(null, null, null, 50, (double) 50);
		Assertions.assertThat(c).isNotIn(jeu.getCartesJeu());
		Assertions.assertThat(jeu.getJoueurs().get(0).getCartesJoueur().get(0).getValeur()).isEqualTo(pioche.getValeur());
	}

}
