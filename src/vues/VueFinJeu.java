package vues;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import enums.TypeCarte;
import metier.Carte;
import metier.Jeu;
import metier.Joueur;

public class VueFinJeu extends JFrame {

	public VueFinJeu(Jeu jeu, int heure, int minute, int time) {

		setTitle("Fin de partie");
		JPanel panel = new JPanel();
		
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Fichier");
		JMenuItem enregistrer = new JMenuItem("Enregistrer");
		menu.add(enregistrer);
		menuBar.add(menu);
		setJMenuBar(menuBar);
		enregistrer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				FileNameExtensionFilter filtre = new FileNameExtensionFilter("Jeu", "jeu");
				fileChooser.addChoosableFileFilter(filtre);
				fileChooser.setFileFilter(filtre);
				fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
				int result = fileChooser.showSaveDialog(fileChooser);
				if (result == JFileChooser.APPROVE_OPTION) {
					try {
						FileOutputStream f = new FileOutputStream(fileChooser.getSelectedFile() + ".jeu");
						ObjectOutputStream s = new ObjectOutputStream(f);
						s.writeObject(jeu);
						s.flush();
						s.close();
					} catch (Exception exception) {
						JOptionPane.showMessageDialog(null, exception.getMessage(), "Erreur",
								JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});
		
		JButton recommencer = new JButton("Recommencer avec les memes joueurs et le meme jeu");
		JButton recommencerPartie = new JButton("Recommencer en changeant les joueurs et de jeu");
		JButton changerJeu = new JButton("Changer de jeu en gardant les m�mes joueurs");
		JButton changerTheme = new JButton("Changer de theme du jeu Cardline en gardant les m�mes joueurs");
		JButton quitter = new JButton("Quitter");
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		JLabel tempsPartie = new JLabel("Temps de la partie: " + heure + ":" + minute + ":" + time);

		c.gridx = 0;
		c.gridy = 0;
		panel.add(tempsPartie, c);
		
		JPanel panelJList = new JPanel();
		panelJList.setBorder(BorderFactory.createTitledBorder(null, "Liste des joueurs",TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION,(new Font("Mshtakan",Font.BOLD,13))));
		DefaultListModel<Joueur> listModel = new DefaultListModel<Joueur>();
		ArrayList<Joueur> arraylistJoueurs  = new ArrayList<Joueur>();
		arraylistJoueurs.addAll(jeu.getJoueurs());
		arraylistJoueurs.addAll(jeu.getJoueursPerdants());
		for (Joueur j : arraylistJoueurs) {
			listModel.addElement(j);
		}

		JList<Joueur> joueurs = new JList<Joueur>(listModel);
		panelJList.add(joueurs);
		
		c.gridx = 0;
		c.gridy = 1;
		panel.add(panelJList, c);

		c.gridx = 0;
		c.gridy = 2;
		panel.add(recommencer, c);

		recommencer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					jeu.setCarteEnJeu(null);
					jeu.setCartesDefausse(new ArrayList<Carte>());
					jeu.setCartesJeu(new ArrayList<Carte>());
					jeu.setCartesPioche(new ArrayList<Carte>());
					ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
					joueurs.addAll(jeu.getJoueurs());
					joueurs.addAll(jeu.getJoueursPerdants());
					jeu.setJoueurs(joueurs);
					jeu.setJoueursPerdants(new ArrayList<Joueur>());
					jeu.setPosJoueur(0);
					jeu.setTempsPartie(0);
					for (Joueur j : jeu.getJoueurs()) {
						j.setCartesJoueur(new ArrayList<Carte>());
						j.setTermine(false);
					}

					dispose();
					VueJeu vueJeu = new VueJeu(jeu);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		c.gridx = 0;
		c.gridy = 3;
		panel.add(recommencerPartie, c);

		recommencerPartie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
				joueurs.addAll(jeu.getJoueurs());
				joueurs.addAll(jeu.getJoueursPerdants());
				for (Joueur j : joueurs) {
					j.setCartesJoueur(new ArrayList<Carte>());
					j.setTermine(false);
				}
				dispose();
				VueSaisieJoueur vueSaisie = new VueSaisieJoueur(joueurs);
			}
		});

		c.gridx = 0;
		c.gridy = 4;
		panel.add(changerJeu, c);

		changerJeu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
				joueurs.addAll(jeu.getJoueurs());
				joueurs.addAll(jeu.getJoueursPerdants());
				jeu.setJoueurs(joueurs);
				jeu.setJoueursPerdants(new ArrayList<Joueur>());
				for (Joueur j : jeu.getJoueurs()) {
					j.setCartesJoueur(new ArrayList<Carte>());
					j.setTermine(false);
				}
				dispose();
				InterfaceChoixJeu vueChoix = new InterfaceChoixJeu(joueurs);
			}
		});

		if (jeu.getTypeJeu() == TypeCarte.cardline) {
			c.gridx = 0;
			c.gridy = 5;
			panel.add(changerTheme, c);
		}

		changerTheme.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
				joueurs.addAll(jeu.getJoueurs());
				joueurs.addAll(jeu.getJoueursPerdants());
				jeu.setJoueurs(joueurs);
				jeu.setJoueursPerdants(new ArrayList<Joueur>());
				for (Joueur j : jeu.getJoueurs()) {
					j.setCartesJoueur(new ArrayList<Carte>());
					j.setTermine(false);
				}
				dispose();
				InterfaceChoixTheme vueChoixTheme = new InterfaceChoixTheme(joueurs);
			}
		});

		c.gridx = 0;
		if (jeu.getTypeJeu() == TypeCarte.cardline) {
			c.gridy = 6;
		} else {
			c.gridy = 4;
		}
		panel.add(quitter, c);

		quitter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(getParent(), "Etes-vous sur de vouloir quitter le jeu ?",
						"Fermeture du jeu ?", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});

		add(panel);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
