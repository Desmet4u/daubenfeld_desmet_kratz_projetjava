package metier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import enums.TypeCarte;

public class test {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		//Joueur j1 = new Joueur("Quentin",1998);
		//Joueur j2 = new Joueur("Julien",1997);
		//Joueur j3 = new Joueur("Antho",1999);
		
		ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
		//joueurs.add(j1);
		//joueurs.add(j2);
		//joueurs.add(j3);
		int nbJoueurs;
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			try {
				System.out.println("Saisir nombre de joueurs:");
				nbJoueurs = Integer.parseInt(sc.nextLine());
				if(nbJoueurs < 9 && nbJoueurs > 1) {
					break;
				}
				else {
					System.out.println("Saisir un entier entre 2 et 8 !!");
				}
			} 
			catch(Exception e) {
				System.out.println("Saisir un entier entre 2 et 8 !!");
			}
		}
		
		String nom;
		//int annee;
		for(int i = 0; i < nbJoueurs; i++) {
			System.out.println("Saisir nom joueur "+(i+1)+":"); //try catch pour vérif
			nom = sc.nextLine();
			
			//System.out.println("Saisir année de naissance joueur "+(i+1)+":"); //try catch pour vérif
			//annee = Integer.parseInt(sc.nextLine());
			
			Joueur j = new Joueur(nom,new Date());
			joueurs.add(j);
			
		}
		
		Jeu jeu = new Jeu(new ArrayList<Joueur>());
		jeu.getJoueurs().addAll(joueurs);
		jeu.trieJoueur();
		jeu.chargerCarte(TypeCarte.timeline,null);
		jeu.distribuerCarte();
		
		
		
		while(true) {
			for(Joueur joueur : jeu.getJoueurs()) {
				System.out.println("Jeu: ");
				
				for(int i=0;i<jeu.getCartesJeu().size();i++) {
					System.out.println(jeu.getCartesJeu().get(i).getValeur());
				}
				
				System.out.println("Joueur "+joueur.getNom()+": ");
				
				for(int i=0;i<joueur.getCartesJoueur().size();i++) {
					System.out.println(joueur.getCartesJoueur().get(i).getValeur());
				}
				
				System.out.println("Quelle carte voulez vous placer ?");
				int posCarteAPlacer = Integer.parseInt(sc.nextLine());
				
				System.out.println("Ou voulez vous placer votre carte ?");
				int pos = Integer.parseInt(sc.nextLine());
				
				System.out.println("A gauche (0) ou à droite (1) ?");
				int gaucheDroite = Integer.parseInt(sc.nextLine());
				
				jeu.ajoutDansJeu(joueur, jeu.getCartesJeu().get(pos), joueur.getCartesJoueur().get(posCarteAPlacer), gaucheDroite);
			}
			
			if(jeu.verifFinPartie() != null) {
				System.out.println("Le joueur "+jeu.verifFinPartie().getNom()+" gagne la partie");
				break;
			}	
		}
		
		//System.out.println("done");
	}

}
