package vues;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import metier.Jeu;
import metier.Joueur;

public class VueLancement extends JFrame {

	public VueLancement() {

		setTitle("Lancement");
		JPanel panel = new JPanel();
		JButton nouvellePartie = new JButton("Nouvelle partie");
		JButton chargerPartie = new JButton("Charger partie");
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;
		panel.add(nouvellePartie, c);
		nouvellePartie.setFont(new Font("Mshtakan", Font.ITALIC, 20));
		nouvellePartie.setPreferredSize(new Dimension(150, 50));
		nouvellePartie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				VueSaisieJoueur vue = new VueSaisieJoueur(null);

			}
		});
		c.gridx = 0;
		c.gridy = 1;
		panel.add(chargerPartie, c);

		chargerPartie.setFont(new Font("Mshtakan", Font.ITALIC, 20));
		chargerPartie.setPreferredSize(new Dimension(150, 50));
		chargerPartie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				FileNameExtensionFilter filtre = new FileNameExtensionFilter("Jeu", "jeu");
				fileChooser.addChoosableFileFilter(filtre);
				fileChooser.setFileFilter(filtre);
				fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
				System.out.println(FileSystemView.getFileSystemView().getHomeDirectory());
				int result = fileChooser.showOpenDialog(fileChooser);
				if (result == JFileChooser.APPROVE_OPTION) {
					try {
						FileInputStream f = new FileInputStream(fileChooser.getSelectedFile());
						ObjectInputStream s = new ObjectInputStream(f);
						Jeu jeu = new Jeu((Jeu) s.readObject());
						// jeu.getJoueurs().forEach(joueur ->
						// fenetre.getPanelEdit().getModelListJoueur().addElement(joueur));
						dispose();
						if (jeu.isFini() == false) {
							VueJeu test = new VueJeu(jeu);
						} else {
							ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
							joueurs.addAll(jeu.getJoueurs());
							joueurs.addAll(jeu.getJoueursPerdants());
							VueSaisieJoueur test2 = new VueSaisieJoueur(joueurs);
						}
						f.close();
						s.close();
					} catch (Exception exception) {
						System.out.println(exception.getMessage());
						JOptionPane.showMessageDialog(null, exception.getMessage(), "Erreur",
								JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});

		JButton ajouterCarte = new JButton("Ajout Carte");
		c.gridx = 0;
		c.gridy = 2;
		panel.add(ajouterCarte, c);

		ajouterCarte.setFont(new Font("Mshtakan", Font.ITALIC, 20));
		ajouterCarte.setPreferredSize(new Dimension(150, 50));
		ajouterCarte.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				vueAjoutCarte test = new vueAjoutCarte();
			}

		});

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});

		add(panel);
		setPreferredSize(new Dimension(250, 200));
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
