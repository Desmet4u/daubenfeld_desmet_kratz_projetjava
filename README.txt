Le jeu est entièrement fonctionnel


Des vérifications pour ajouter carte et lors de la saisie du joueur ont été faite

Les tests unitaires ont été fait mais nécessite l'ajout de la librairie JUnitTest5 ainsi que assertj-core-3.11.1 qui se trouve dans le dossier data

L'application nécessite aussi la librairie jdatepicker-1.3.4 qui se trouve dans le dossier data

Toutes les fonctionnalités ont été programmés


Pour placer une carte sur le jeu: cliquer sur une carte de votre main puis cliquer sur le bouton + qui semble etre la bonne place dans le jeu