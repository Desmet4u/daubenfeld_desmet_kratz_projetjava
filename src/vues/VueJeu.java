package vues;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import enums.DifficulteBot;
import enums.ThemeCarte;
import enums.TypeCarte;
import metier.Carte;
import metier.Jeu;
import metier.Joueur;

@SuppressWarnings("serial")
public class VueJeu extends JFrame {
	private JPanel paneJoueur;
	private JPanel paneTapis = new JPanel();
	private JScrollPane paneTest;
	private JPanel panelInfos;
	private Jeu partieLocal = new Jeu();
	private ArrayList<JButton> gaucheDroite = new ArrayList<JButton>();
	private ArrayList<JButton> CarteTapis = new ArrayList<JButton>();
	private ArrayList<JButton> carteMain = new ArrayList<JButton>();
	private int choixMain = -1;
	private int clickCarte = -1;
	private boolean fina = false;
	int temps;
	int time;
	Timer jolieTimer = null;
	Timer timerPartie = null;
	int heure = 0, minute = 0;

	public VueJeu(Jeu jeu) throws IOException {

		if (jeu.getTypeJeu() == TypeCarte.timeline) {
			paneJoueur = new JPanel() {
				ImageIcon icon = new ImageIcon("Data/image/21-31.jpg");
				Image image = icon.getImage().getScaledInstance(1500, 170, Image.SCALE_DEFAULT);
				{
					this.setOpaque(false);
				}

				public void paintComponent(Graphics g) {
					g.drawImage(image, 0, 0, this);
					super.paintComponent(g);
				}
			};

			paneTest = new JScrollPane(paneTapis) {
				ImageIcon icon = new ImageIcon("Data/image/21-21.jpg");
				Image image = icon.getImage().getScaledInstance(1500, 450, Image.SCALE_DEFAULT);
				{
					this.setOpaque(false);
					paneTapis.setBackground(new Color(0, 0, 0, 64));
				}

				public void paintComponent(Graphics g) {
					g.drawImage(image, 0, 0, this);
					super.paintComponent(g);
				}
			};

			panelInfos = new JPanel() {
				ImageIcon icon = new ImageIcon("Data/image/21-11.jpg");
				Image image = icon.getImage().getScaledInstance(1500, 170, Image.SCALE_DEFAULT);
				{
					this.setOpaque(false);
				}

				public void paintComponent(Graphics g) {
					g.drawImage(image, 0, 0, this);
					super.paintComponent(g);
				}
			};
		}
		else {
			paneJoueur = new JPanel() {
				ImageIcon icon = new ImageIcon("Data/image/28-31.jpg");
				Image image = icon.getImage().getScaledInstance(1500, 170, Image.SCALE_DEFAULT);
				{
					this.setOpaque(false);
				}

				public void paintComponent(Graphics g) {
					g.drawImage(image, 0, 0, this);
					super.paintComponent(g);
				}
			};

			paneTest = new JScrollPane(paneTapis) {
				ImageIcon icon = new ImageIcon("Data/image/28-21.jpg");
				Image image = icon.getImage().getScaledInstance(1500, 450, Image.SCALE_DEFAULT);
				{
					this.setOpaque(false);
					paneTapis.setBackground(new Color(0, 0, 0, 64));
				}

				public void paintComponent(Graphics g) {
					g.drawImage(image, 0, 0, this);
					super.paintComponent(g);
				}
			};

			panelInfos = new JPanel() {
				ImageIcon icon = new ImageIcon("Data/image/28-11.jpg");
				Image image = icon.getImage().getScaledInstance(1500, 170, Image.SCALE_DEFAULT);
				{
					this.setOpaque(false);
				}

				public void paintComponent(Graphics g) {
					g.drawImage(image, 0, 0, this);
					super.paintComponent(g);
				}
			};
		}

		String soundName = "Data/sounds/melange.wav";
		AudioInputStream audioInputStream = null;

		try {
			audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (UnsupportedAudioFileException | IOException e1) {

		} catch (LineUnavailableException e1) {

		}

		paneJoueur.setBackground(new Color(0, 0, 0, 64));
		panelInfos.setBackground(new Color(0, 0, 0, 64));
		ArrayList<Joueur> listBot = new ArrayList<Joueur>();
		int nbBot = 0;
		for (Joueur j : jeu.getJoueurs()) {
			if (j.isBot()) {
				listBot.add(j);
				nbBot++;
			}
		}

		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Fichier");
		JMenu menu1 = new JMenu("Bot");
		JMenu menu2 = new JMenu("Aide");
		JMenu menu3 = null;
		if (jeu.getTypeJeu() == TypeCarte.cardline) {
			menu3 = new JMenu("Theme");
			JMenuItem superficie = new JMenuItem("Superficie");
			JMenuItem population = new JMenuItem("Population");
			JMenuItem pib = new JMenuItem("PIB");
			JMenuItem pollution = new JMenuItem("Pollution");

			menu3.add(superficie);
			menu3.add(population);
			menu3.add(pib);
			menu3.add(pollution);

			superficie.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					jeu.changementTheme(ThemeCarte.superficie);
					partieLocal = jeu;
					afficheCarteTapis();
					paneTapis.revalidate();
					paneTapis.repaint();

					afficherInfosJoueur();
				}
			});

			population.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					jeu.changementTheme(ThemeCarte.population);
					partieLocal = jeu;
					afficheCarteTapis();
					paneTapis.revalidate();
					paneTapis.repaint();

					afficherInfosJoueur();
				}
			});

			pib.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					jeu.changementTheme(ThemeCarte.pib);
					partieLocal = jeu;
					afficheCarteTapis();
					paneTapis.revalidate();
					paneTapis.repaint();

					afficherInfosJoueur();
				}
			});

			pollution.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					jeu.changementTheme(ThemeCarte.pollution);
					partieLocal = jeu;
					afficheCarteTapis();
					paneTapis.revalidate();
					paneTapis.repaint();

					afficherInfosJoueur();
				}
			});
		}
		JMenuItem enregistrer = new JMenuItem("Enregistrer");

		ArrayList<JMenuItem> arrayMenuBot = new ArrayList<JMenuItem>();

		for (int i = 0; i < nbBot; i++) {
			JMenu bot = new JMenu("Bot" + i);
			arrayMenuBot.add(bot);
			menu1.add(bot);

			JMenuItem deb = new JMenuItem("Débutant");
			JMenuItem cult = new JMenuItem("Cultivé");
			JMenuItem gen = new JMenuItem("Génie");

			bot.add(deb);
			bot.add(cult);
			bot.add(gen);

			deb.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					int pos = arrayMenuBot.indexOf(bot);
					int posJoueur = jeu.getJoueurs().indexOf(listBot.get(pos));
					jeu.getJoueurs().get(posJoueur).setDifficulte(DifficulteBot.debutant);
				}
			});

			cult.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					int pos = arrayMenuBot.indexOf(bot);
					int posJoueur = jeu.getJoueurs().indexOf(listBot.get(pos));
					jeu.getJoueurs().get(posJoueur).setDifficulte(DifficulteBot.cultive);
				}
			});

			gen.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					int pos = arrayMenuBot.indexOf(bot);
					int posJoueur = jeu.getJoueurs().indexOf(listBot.get(pos));
					jeu.getJoueurs().get(posJoueur).setDifficulte(DifficulteBot.genie);
				}
			});
		}

		JMenuItem aide = new JMenuItem("Règle");
		JMenuItem joker = new JMenuItem("Joker");
		enregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.META_DOWN_MASK));
		menu.add(enregistrer);
		menu2.add(aide);
		menu2.add(joker);
		menuBar.add(menu);
		menuBar.add(menu1);
		if (jeu.getTypeJeu() == TypeCarte.cardline) {
			menuBar.add(menu3);
		}
		menuBar.add(menu2);
		setJMenuBar(menuBar);

		enregistrer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				FileNameExtensionFilter filtre = new FileNameExtensionFilter("Jeu", "jeu");
				fileChooser.addChoosableFileFilter(filtre);
				fileChooser.setFileFilter(filtre);
				fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
				int result = fileChooser.showSaveDialog(fileChooser);
				if (result == JFileChooser.APPROVE_OPTION) {
					try {
						FileOutputStream f = new FileOutputStream(fileChooser.getSelectedFile() + ".jeu");
						ObjectOutputStream s = new ObjectOutputStream(f);
						s.writeObject(jeu);
						s.flush();
						s.close();
					} catch (Exception exception) {
						JOptionPane.showMessageDialog(null, exception.getMessage(), "Erreur",
								JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});

		aide.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				File f;
				if (jeu.getTypeJeu() == TypeCarte.timeline) {
					f = new File("data/regle.pdf");
				} else {
					f = new File("data/cardline_regle.pdf");
				}
				try {
					Desktop.getDesktop().open(f);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		joker.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (jeu.getJoueurs().get(jeu.getPosJoueur()).getJoker() == 1) {
					Carte carte = jeu.getJoueurs().get(jeu.getPosJoueur()).choixCarteBot();
					JButton button = (JButton) carteMain
							.get(jeu.getJoueurs().get(jeu.getPosJoueur()).getCartesJoueur().indexOf(carte));

					int posBouton = carteMain.indexOf(button);
					Carte c = partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).getCartesJoueur().get(posBouton);
					ImageIcon imageIcon = new ImageIcon(
							"data/" + partieLocal.getTypeJeu().toString() + "/cards/" + partieLocal.getJoueurs()
									.get(partieLocal.getPosJoueur()).getCartesJoueur().get(posBouton).getImageCachee());

					button.setIcon(
							new ImageIcon(imageIcon.getImage().getScaledInstance(220, 220, Image.SCALE_DEFAULT)));

					Integer bonnePos = null;
					int i = 0;
					while (bonnePos == null) {
						if (jeu.getCartesJeu().get(i).getValeur() > carte.getValeur()) {
							bonnePos = i;
						} else if (jeu.getCartesJeu().size() - 1 == i) {
							bonnePos = i + 1;
						} else {
							i++;
						}
					}

					gaucheDroite.get(bonnePos).setText("ICI");
					jeu.getJoueurs().get(jeu.getPosJoueur()).setJoker(0);

				} else {
					JOptionPane.showMessageDialog(null, "Vous n'avez plus de joker !", "Information",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}

		});

		this.pack();
		this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);

		partieLocal = jeu;

		temps = 0;
		int delay = 1000; // milliseconds
		setVisible(true);

		time = partieLocal.getTempsPartie();
		while (time > 3600) {
			heure++;
			time = time - 3600;
		}
		while (time > 60) {
			minute++;
			time = time - 60;
		}
		timerPartie = new Timer(delay, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				time++;
				int test = partieLocal.getTempsPartie();
				test++;
				partieLocal.setTempsPartie(test);
				if (time == 60) {
					minute++;
					time = 0;
				}
				if (minute == 60) {
					heure++;
					minute = 0;
				}
			}

		});
		timerPartie.start();

		jolieTimer = new Timer(delay, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				temps++;
				if (temps == 25) {
					String soundName = "Data/sounds/tictac.wav";
					AudioInputStream audioInputStream = null;

					try {
						audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
						Clip clip = AudioSystem.getClip();
						clip.open(audioInputStream);
						clip.start();
					} catch (UnsupportedAudioFileException | IOException e1) {

					} catch (LineUnavailableException e1) {

					}
				}
				if (temps == 30) {
					if (partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).isBot() == false) {
						finDePartie();
						if (fina == false) {
							if (partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).isBot() == false
									&& partieLocal.getJoueurs().size() > 1) {
								temps = 0;
								afficheCarteTapis();
								paneTapis.revalidate();
								paneTapis.repaint();

								afficheCarteMain();
								paneJoueur.revalidate();
								paneJoueur.repaint();

								afficherInfosJoueur();
							}
						}
					}
					temps = 0;
					partieLocal.setPosJoueur((partieLocal.getPosJoueur() + 1) % (partieLocal.getJoueurs().size()));
					if (partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).isBot()) {
						gestionBot();
						if (partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).isBot() == false
								&& partieLocal.getJoueurs().size() > 1) {
							temps = 0;
							afficheCarteTapis();
							paneTapis.revalidate();
							paneTapis.repaint();

							afficheCarteMain();
							paneJoueur.revalidate();
							paneJoueur.repaint();

							afficherInfosJoueur();
						}
					}
					//gere l'erreur si une carte est sélectionnée
					afficheCarteMain();
					paneJoueur.revalidate();
					paneJoueur.repaint();
				}
				if (fina == false) {
					afficherInfosJoueur();
				}
			}
		});
		jolieTimer.start();

		if (partieLocal.getCartesJeu().size() == 0) {
			partieLocal.chargerCarte(partieLocal.getTypeJeu(), partieLocal.getThemeJeu());
			partieLocal.distribuerCarte();
			partieLocal.trieJoueur();
		}

		this.setTitle("Jeu: " + partieLocal.getTypeJeu().toString());

		this.setLayout(new BorderLayout());
		panelInfos.setLayout(new BorderLayout());

		while (partieLocal.getJoueurs().size() > 1
				&& partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).isBot() == true) {
			gestionBot();
		}

		if (partieLocal.getJoueurs().size() > 1) {
			afficheCarteTapis();
			afficheCarteMain();
			afficherInfosJoueur();

			this.setVisible(true);
		}

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(getParent(), "Êtes-vous sur de vouloir quitter le jeu ?",
						"Fermeture du jeu ?", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});
	}

	private void gestionBot() {

		try {

			if (partieLocal.getJoueurs().size() > 1 && fina == false) {

				Carte carte = partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).choixCarteBot();
				partieLocal.choixPlacementCarteBot(partieLocal.getJoueurs().get(partieLocal.getPosJoueur()), carte);
			}

			finDePartie();

			partieLocal.setPosJoueur((partieLocal.getPosJoueur() + 1) % (partieLocal.getJoueurs().size()));

		} catch (Exception e) {

		}

	}

	public void afficheCarteTapis() {
		paneTapis.removeAll();
		JButton gauche = new JButton("+");
		gauche.setActionCommand("0");
		gauche.addActionListener(new clickGaucheDroite());
		gaucheDroite.clear();
		gaucheDroite.add(gauche);
		paneTapis.add(gauche);
		gauche.setVisible(true);
		gauche.setEnabled(false);
		gauche.setPreferredSize(new Dimension(40, 40));
		CarteTapis.clear();

		int n;
		for (int i = 0; i < partieLocal.getCartesJeu().size(); i++) {
			ImageIcon imageIcon = new ImageIcon("data/" + partieLocal.getTypeJeu().toString() + "/cards/"
					+ partieLocal.getCartesJeu().get(i).getImageVisible()); 
			Image image = imageIcon.getImage();
			Image newimg = image.getScaledInstance(200, 300, java.awt.Image.SCALE_SMOOTH);
			imageIcon = new ImageIcon(newimg);
			JButton button = new JButton();
			button.setIcon(imageIcon);
			button.setActionCommand(Integer.toString(i));
			button.setEnabled(true);
			button.setDisabledIcon(imageIcon);
			CarteTapis.add(button);
			button.addActionListener(new clickTapis());
			paneTapis.add(button);
			JButton droite = new JButton("+");
			n = i + 1;

			droite.setActionCommand(Integer.toString(n));
			droite.addActionListener(new clickGaucheDroite());
			droite.setPreferredSize(new Dimension(40, 40));
			gaucheDroite.add(droite);
			paneTapis.add(droite);
			droite.setVisible(true);
			droite.setEnabled(false);
		}
		this.add(paneTest, BorderLayout.CENTER);
	}

	public void afficheCarteMain() {
		paneJoueur.removeAll();
		carteMain = new ArrayList<JButton>();
		for (int i = 0; i < partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).getCartesJoueur().size(); i++) {
			ImageIcon imageIcon = new ImageIcon("data/" + partieLocal.getTypeJeu().toString() + "/cards/" + partieLocal
					.getJoueurs().get(partieLocal.getPosJoueur()).getCartesJoueur().get(i).getImageCachee());
			Image image = imageIcon.getImage();
			Image newimg = image.getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
			imageIcon = new ImageIcon(newimg);
			JButton button = new JButton();
			button.setIcon(imageIcon);
			button.setActionCommand(Integer.toString(i));
			carteMain.add(button);
			button.addActionListener(new clickMain());
			paneJoueur.add(button);

			paneJoueur.setSize(getWidth(), button.getHeight());

			button.addMouseListener(new MouseListener() {

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generatednmethod stub

				}

				@Override
				public void mouseEntered(MouseEvent e) {
					JButton button = (JButton) e.getSource();

					int posBouton = carteMain.indexOf(button);
					Carte c = partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).getCartesJoueur().get(posBouton);
					ImageIcon imageIcon = new ImageIcon(
							"data/" + partieLocal.getTypeJeu().toString() + "/cards/" + partieLocal.getJoueurs()
									.get(partieLocal.getPosJoueur()).getCartesJoueur().get(posBouton).getImageCachee());

					button.setIcon(
							new ImageIcon(imageIcon.getImage().getScaledInstance(220, 220, Image.SCALE_DEFAULT)));
				}

				@Override
				public void mouseExited(MouseEvent e) {
					if (clickCarte == -1) {
						JButton button = (JButton) e.getSource();

						int posBouton = carteMain.indexOf(button);
						Carte c = partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).getCartesJoueur()
								.get(posBouton);
						ImageIcon imageIcon = new ImageIcon("data/" + partieLocal.getTypeJeu().toString() + "/cards/"
								+ partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).getCartesJoueur()
										.get(posBouton).getImageCachee());

						button.setIcon(
								new ImageIcon(imageIcon.getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT)));
					}
					clickCarte = -1;

				}

				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub

				}
			});
			this.add(paneJoueur, BorderLayout.SOUTH);
		}
	}

	public void afficherInfosJoueur() {

		try {
			panelInfos.removeAll();
			JPanel infosTexte = new JPanel();
			JPanel infosDefausse = new JPanel();
			JPanel infosPioche = new JPanel();

			infosTexte.setBackground(new Color(0, 0, 0, 64));
			infosDefausse.setBackground(new Color(0, 0, 0, 64));
			infosPioche.setBackground(new Color(0, 0, 0, 64));

			infosTexte.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			this.setBackground(Color.BLUE);

			c.fill = GridBagConstraints.BOTH;
			c.insets = new Insets(2, 2, 2, 2);
			c.gridx = 0;
			c.gridy = 0;
			JLabel nom = new JLabel("Joueur: " + partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).getNom());

			nom.setFont(new Font("Serif", Font.BOLD, 18));
			infosTexte.add(nom, c);

			JLabel nbCartes = new JLabel("Nombre de cartes restantes: "
					+ partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).getCartesJoueur().size());
			nbCartes.setFont(new Font("Serif", Font.BOLD, 18));
			c.gridy = 1;
			infosTexte.add(nbCartes, c);

			JLabel temps = new JLabel("temps restant: " + (30 - this.temps));
			temps.setFont(new Font("Serif", Font.BOLD, 18));
			c.gridx = 0;
			c.gridy = 2;
			infosTexte.add(temps, c);

			JLabel time = new JLabel("Temps partie: " + this.heure + ":" + this.minute + ":" + this.time);
			time.setFont(new Font("Serif", Font.BOLD, 18));
			c.gridx = 0;
			c.gridy = 3;
			infosTexte.add(time, c);

			if (partieLocal.getTypeJeu() == TypeCarte.cardline) {
				JLabel type = new JLabel("Theme jeu : " + partieLocal.getThemeJeu());
				type.setFont(new Font("Serif", Font.BOLD, 18));
				c.gridx = 0;
				c.gridy = 4;
				infosTexte.add(type, c);
			}

			c.gridx = 1;
			c.gridy = 0;
			c.gridheight = partieLocal.getJoueurs().size();
			infosTexte.add(new JSeparator(SwingConstants.VERTICAL), c);

			c.gridx = 2;
			c.gridy = 0;
			c.gridheight = 1;

			for (int i = 0; i < partieLocal.getJoueurs().size(); i++) {
				c.gridy = i;
				JLabel otherPlayers = new JLabel("Joueur " + partieLocal.getJoueurs().get(i).getNom() + ": "
						+ partieLocal.getJoueurs().get(i).getCartesJoueur().size() + " cartes");
				otherPlayers.setFont(new Font("Serif", Font.ITALIC, 18));
				infosTexte.add(otherPlayers, c);

				infosTexte.add(otherPlayers, c);

			}

			infosDefausse.setLayout(new BorderLayout());

			JLabel defausse = new JLabel("Défausse");
			defausse.setHorizontalAlignment(JLabel.CENTER);
			infosDefausse.add(defausse, BorderLayout.NORTH);
			if (!partieLocal.getCartesDefausse().isEmpty()) {
				ImageIcon imageIcon = new ImageIcon(
						"data/" + partieLocal.getTypeJeu().toString() + "/cards/" + partieLocal.getCartesDefausse()
								.get(partieLocal.getCartesDefausse().size() - 1).getImageVisible());

				Image image = imageIcon.getImage();
				Image newimg = image.getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
				imageIcon = new ImageIcon(newimg);
				JLabel label = new JLabel();
				label.setIcon(imageIcon);
				infosDefausse.add(label, BorderLayout.CENTER);
			}

			infosPioche.setLayout(new BorderLayout());

			JLabel pioche = new JLabel("Pioche");
			pioche.setHorizontalAlignment(JLabel.CENTER);
			infosPioche.add(pioche, BorderLayout.NORTH);
			if (partieLocal.getCartesPioche() != null) {
				ImageIcon imageIcon = new ImageIcon("data/" + partieLocal.getTypeJeu().toString() + "/cards/"
						+ partieLocal.getCartesPioche().get(partieLocal.getCartesPioche().size() - 1).getImageCachee());

				Image image = imageIcon.getImage();
				Image newimg = image.getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
				imageIcon = new ImageIcon(newimg);
				JLabel label = new JLabel();
				label.setIcon(imageIcon);
				infosPioche.add(label, BorderLayout.CENTER);
			}

			panelInfos.add(infosTexte, BorderLayout.CENTER);
			panelInfos.add(infosDefausse, BorderLayout.EAST);
			panelInfos.add(infosPioche, BorderLayout.WEST);

			this.add(panelInfos, BorderLayout.NORTH);
			panelInfos.revalidate();
			panelInfos.repaint();
		} catch (Exception e) {

		}
	}

	class clickMain implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			JButton button1 = (JButton) e.getSource();
			int posBouton = carteMain.indexOf(button1);
			clickCarte = posBouton;

			choixMain = Integer.parseInt(e.getActionCommand());

			String soundName = "Data/sounds/card.wav";
			AudioInputStream audioInputStream = null;

			try {
				audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
				Clip clip = AudioSystem.getClip();
				clip.open(audioInputStream);
				clip.start();
			} catch (UnsupportedAudioFileException | IOException e1) {

			} catch (LineUnavailableException e1) {

			}
			for (JButton button : gaucheDroite)
				button.setEnabled(true);
		}
	}

	class clickTapis implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			JButton button = (JButton) e.getSource();

			int posBouton = CarteTapis.indexOf(button);
			ImageIcon imageIcon = new ImageIcon("data/" + partieLocal.getTypeJeu().toString() + "/cards/"
					+ partieLocal.getCartesJeu().get(posBouton).getImageVisible());

			ImageIcon testImage = new ImageIcon(imageIcon.getImage().getScaledInstance(300, 300, Image.SCALE_DEFAULT));

			JFrame test = new JFrame();
			JButton bouton = new JButton(testImage);

			test.add(bouton);
			test.pack();
			test.setLocationRelativeTo(null);
			test.setVisible(true);

		}
	}

	class clickGaucheDroite implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int posCarteTapis = Integer.parseInt(e.getActionCommand());
			for (int i = 0; i < gaucheDroite.size(); i++) {
				gaucheDroite.get(i).setVisible(false);
			}
			for (int i = 0; i < CarteTapis.size(); i++) {
				CarteTapis.get(i).setEnabled(false);
			}
			Joueur j = partieLocal.getJoueurs().get(partieLocal.getPosJoueur());
			partieLocal.ajoutDansJeuTest(j, posCarteTapis,
					partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).getCartesJoueur().get(choixMain));

			finDePartie();

			partieLocal.setPosJoueur((partieLocal.getPosJoueur() + 1) % (partieLocal.getJoueurs().size()));

			while (partieLocal.getJoueurs().size() > 1
					&& partieLocal.getJoueurs().get(partieLocal.getPosJoueur()).isBot() == true) {
				gestionBot();
			}

			if (partieLocal.getJoueurs().size() > 1) {
				temps = 0;
				afficheCarteTapis();
				paneTapis.revalidate();
				paneTapis.repaint();

				afficheCarteMain();
				paneJoueur.revalidate();
				paneJoueur.repaint();

				afficherInfosJoueur();
			}
		}
	}

	public void finDePartie() {
		if (partieLocal.getPosJoueur() == partieLocal.getJoueurs().size() - 1 && fina == false) {

			if ((partieLocal.getCartesPioche().size() + partieLocal.getCartesDefausse().size()) < partieLocal
					.getJoueurs().size()) {
				String gagnants = "";
				for (Joueur joueur : partieLocal.getJoueurs()) {
					gagnants = gagnants + joueur.getNom() + ", ";
				}
				System.out.println("pioche: " + partieLocal.getCartesPioche().size());
				System.out.println("defausse: " + partieLocal.getCartesDefausse().size());
				JOptionPane.showMessageDialog(null, "Les joueurs " + gagnants + " sont à égalité !!", "Egalité !",
						JOptionPane.INFORMATION_MESSAGE);
				String soundName = "Data/sounds/lost.wav";
				AudioInputStream audioInputStream = null;

				try {
					audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
					Clip clip = AudioSystem.getClip();
					clip.open(audioInputStream);
					clip.start();
				} catch (UnsupportedAudioFileException | IOException e1) {

				} catch (LineUnavailableException e1) {

				}
				fina = true;
				partieLocal.setFini(true);
				jolieTimer.stop();
				timerPartie.stop();
				dispose();
				VueFinJeu fin = new VueFinJeu(partieLocal, heure, minute, time);
			} else {

				Joueur gagnant = partieLocal.verifFinPartie();

				if (gagnant != null) {
					JOptionPane.showMessageDialog(null, "Le joueur " + gagnant.getNom() + " gagne la partie !!",
							"Victoire !!!", JOptionPane.INFORMATION_MESSAGE);
					String soundName = "Data/sounds/tambour.wav";
					AudioInputStream audioInputStream = null;

					try {
						audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
						Clip clip = AudioSystem.getClip();
						clip.open(audioInputStream);
						clip.start();
					} catch (UnsupportedAudioFileException | IOException e1) {

					} catch (LineUnavailableException e1) {

					}
					fina = true;
					partieLocal.setFini(true);
					int score = gagnant.getScore();
					gagnant.setScore(score + 1);
					jolieTimer.stop();
					timerPartie.stop();
					dispose();
					VueFinJeu fin = new VueFinJeu(partieLocal, heure, minute, time);
				}
			}
		}
	}
}
