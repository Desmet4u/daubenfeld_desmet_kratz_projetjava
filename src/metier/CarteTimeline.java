package metier;

public class CarteTimeline extends Carte {
	/**
	 * attribut de CardeTimeline : date
	 */
	private int date;

	/**
	 * Constructeur de l'objet CarteTimeline avec l'attribut date
	 * @param invention
	 * @param imageCachee
	 * @param imageVisible
	 * @param date
	 * @param valeur
	 */
	public CarteTimeline(String invention, String imageCachee, String imageVisible, int date, Double valeur) {
		super(invention, imageCachee, imageVisible, valeur);
		this.date = date;
	}

	/**
	 * methodes retournant ou modifiant l'attribut Date de la classe Carte
	 * @return
	 */
	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}
}
