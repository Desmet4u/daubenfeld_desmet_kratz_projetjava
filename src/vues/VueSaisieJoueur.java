package vues;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import enums.DifficulteBot;
import metier.Carte;
import metier.Joueur;

public class VueSaisieJoueur extends JFrame {

	private int numero;

	public VueSaisieJoueur(ArrayList<Joueur> joueursTest) {
		setTitle("Saisie Joueurs");
		JPanel panel = new JPanel();

		JPanel joueursPanel = new JPanel();
		JPanel botsPanel = new JPanel();
		JPanel jListPanel = new JPanel();

		panel.setLayout(new GridBagLayout());
		joueursPanel.setLayout(new GridBagLayout());
		botsPanel.setLayout(new GridBagLayout());
		jListPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;
		panel.add(joueursPanel, c);
		c.gridx = 2;
		panel.add(botsPanel, c);
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		panel.add(jListPanel, c);

		joueursPanel.setBorder(BorderFactory.createTitledBorder(null, "Joueurs", TitledBorder.CENTER,
				TitledBorder.DEFAULT_POSITION, (new Font("Mshtakan", Font.BOLD, 13))));
		botsPanel.setBorder(BorderFactory.createTitledBorder(null, "Bots", TitledBorder.CENTER,
				TitledBorder.DEFAULT_POSITION, (new Font("Mshtakan", Font.BOLD, 13))));
		jListPanel.setBorder(BorderFactory.createTitledBorder(null, "Liste des joueurs", TitledBorder.CENTER,
				TitledBorder.DEFAULT_POSITION, (new Font("Mshtakan", Font.BOLD, 13))));

		// info.setPreferredSize(new Dimension(150, 25));
		JButton buttonJouer = new JButton("Jouer");
		buttonJouer.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		if (joueursTest != null) {
			buttonJouer.setEnabled(true);
		} else {
			buttonJouer.setEnabled(false);
		}
		JButton supprimerJoueur = new JButton("Supprimer");
		supprimerJoueur.setFont(new Font("Mshtakan", Font.ITALIC, 12));

		int nbBot = 0;
		if (joueursTest != null) {
			for (Joueur j : joueursTest) {
				if (j.isBot()) {
					nbBot++;
				}
			}
			numero = nbBot;
		}

		JButton ajouterBotDebutant = new JButton("D�butant");
		JButton ajouterBotCultive = new JButton("Cultiv�");
		JButton ajouterBotGenie = new JButton("G�nie");
		JLabel label_nom = new JLabel("Nom");
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		joueursPanel.add(label_nom, c);
		JLabel label_date = new JLabel("Age");
		JDatePickerImpl datePicker = new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), new Properties()),
				new DateLabelFormatter());

		c.gridy = 1;
		joueursPanel.add(label_date, c);

		JTextField nom = new JTextField();
		c.gridx = 1;
		c.gridy = 0;
		// c.gridwidth=2;
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridwidth = GridBagConstraints.REMAINDER;
		nom.setPreferredSize(new Dimension(100, 25));
		joueursPanel.add(nom, c);

		c.gridy = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		joueursPanel.add(datePicker, c);
		datePicker.setPreferredSize(new Dimension(150, 25));

		DefaultListModel<Joueur> listModel = new DefaultListModel<Joueur>();
		ArrayList<Joueur> arraylistJoueurs = new ArrayList<Joueur>();

		if (joueursTest != null) {
			for (Joueur j : joueursTest) {
				listModel.addElement(j);
				arraylistJoueurs.add(j);
			}
		}

		JList<Joueur> joueurs = new JList<Joueur>(listModel);
		joueurs.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				supprimerJoueur.setEnabled(true);
			}
		});

		JButton ajouterJoueur = new JButton("Ajouter");

		joueurs.setPreferredSize(new Dimension(200, 20 * 8));
		c.gridx = 0;
		c.gridy = 0;
		jListPanel.add(joueurs, c);

		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		joueursPanel.add(ajouterJoueur, c);
		ajouterJoueur.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date date = null;
				try {
					date = new SimpleDateFormat("dd/MM/yyyy").parse(datePicker.getJFormattedTextField().getText());
				} catch (ParseException e1) {

				}

				int suite = 1;
				if (date != null && !nom.getText().equals("")) {
					Joueur j = new Joueur(nom.getText(), date);

					for (Joueur test : arraylistJoueurs) {
						if (test.getNom().equals(j.getNom())) {
							suite = 0;
						}
					}
					
					if (suite == 1) {
						datePicker.getJFormattedTextField().setText("");
						nom.setText("");

						listModel.addElement(j);
						arraylistJoueurs.add(j);
						if (arraylistJoueurs.size() > 1) {
							buttonJouer.setEnabled(true);
						}
						if (arraylistJoueurs.size() == 8) {
							ajouterJoueur.setEnabled(false);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Un joueur possède le même nom, veuillez changer",
								"Informations", JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}
		});

		c.gridx = 0;
		c.gridy = 0;
		botsPanel.add(ajouterBotDebutant, c);
		ajouterBotDebutant.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Joueur j = new Joueur(numero, DifficulteBot.debutant);
				listModel.addElement(j);
				arraylistJoueurs.add(j);
				numero++;
				if (arraylistJoueurs.size() > 1) {
					buttonJouer.setEnabled(true);
				}
				if (arraylistJoueurs.size() == 8) {
					ajouterJoueur.setEnabled(false);
					ajouterBotDebutant.setEnabled(false);
					ajouterBotCultive.setEnabled(false);
					ajouterBotGenie.setEnabled(false);
				}
			}
		});

		c.gridx = 0;
		c.gridy = 1;
		botsPanel.add(ajouterBotCultive, c);
		ajouterBotCultive.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Joueur j = new Joueur(numero, DifficulteBot.cultive);
				listModel.addElement(j);
				arraylistJoueurs.add(j);
				numero++;
				if (arraylistJoueurs.size() > 1) {
					buttonJouer.setEnabled(true);
				}
				if (arraylistJoueurs.size() == 8) {
					ajouterJoueur.setEnabled(false);
					ajouterBotDebutant.setEnabled(false);
					ajouterBotCultive.setEnabled(false);
					ajouterBotGenie.setEnabled(false);
				}
			}
		});

		c.gridx = 0;
		c.gridy = 2;
		botsPanel.add(ajouterBotGenie, c);
		ajouterBotGenie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Joueur j = new Joueur(numero, DifficulteBot.genie);
				listModel.addElement(j);
				arraylistJoueurs.add(j);
				numero++;
				if (arraylistJoueurs.size() > 1) {
					buttonJouer.setEnabled(true);
				}
				if (arraylistJoueurs.size() == 8) {
					ajouterJoueur.setEnabled(false);
					ajouterBotDebutant.setEnabled(false);
					ajouterBotCultive.setEnabled(false);
					ajouterBotGenie.setEnabled(false);
				}
			}
		});

		supprimerJoueur.setEnabled(false);
		c.gridx = 0;
		c.gridy = 3;
		c.weightx = 2;
		c.gridwidth = GridBagConstraints.REMAINDER;
		jListPanel.add(supprimerJoueur, c);
		supprimerJoueur.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Joueur j = listModel.getElementAt(joueurs.getSelectedIndex());
				listModel.removeElement(j);
				arraylistJoueurs.remove(j);

				supprimerJoueur.setEnabled(false);
				if (arraylistJoueurs.size() > 1) {
					buttonJouer.setEnabled(true);
				} else {
					buttonJouer.setEnabled(false);
				}
				if (arraylistJoueurs.size() == 8) {
					ajouterJoueur.setEnabled(false);
					ajouterBotDebutant.setEnabled(false);
					ajouterBotCultive.setEnabled(false);
					ajouterBotGenie.setEnabled(false);
				} else {
					ajouterJoueur.setEnabled(true);
					ajouterBotDebutant.setEnabled(true);
					ajouterBotCultive.setEnabled(true);
					ajouterBotGenie.setEnabled(true);
				}

			}
		});

		c.gridx = 0;
		c.gridy = 5;
		c.fill = GridBagConstraints.HORIZONTAL;
		jListPanel.add(buttonJouer, c);

		buttonJouer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (arraylistJoueurs.size() < 2) {
					int nb = 2 - arraylistJoueurs.size();
					JOptionPane.showMessageDialog(null, "Ajouter au moins " + nb + " joueurs", "Informations",
							JOptionPane.INFORMATION_MESSAGE);
				} else if (arraylistJoueurs.size() > 8) {
					int nb = arraylistJoueurs.size() - 8;
					JOptionPane.showMessageDialog(null, "Retirer au moins " + nb + " joueurs", "Informations",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					dispose();
					for (Joueur j : arraylistJoueurs) {
						j.setCartesJoueur(new ArrayList<Carte>());
						j.setTermine(false);
					}
					InterfaceChoixJeu choixJeu = new InterfaceChoixJeu(arraylistJoueurs);
				}

			}
		});

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(getParent(), "Êtes-vous sur de vouloir quitter le jeu ?",
						"Fermeture du jeu ?", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});

		ajouterBotCultive.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		ajouterBotDebutant.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		ajouterBotGenie.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		ajouterJoueur.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		buttonJouer.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		jListPanel.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		label_date.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		label_nom.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		nom.setFont(new Font("Mshtakan", Font.ITALIC, 12));
		datePicker.getJFormattedTextField().setFont(new Font("Mshtakan", Font.ITALIC, 12));

		add(panel);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
