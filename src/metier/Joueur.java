package metier;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import enums.DifficulteBot;

public class Joueur implements Serializable{
	
	/**
	 * attributs de la classe Joueur : nom, dateDeNaissance, liste des cartes des joueurs, bool�en termin�
	 */
	private String nom;
	private Date dateDeNaissance;
	private ArrayList<Carte> cartesJoueur;
	private boolean termine;
	private boolean bot;
	private DifficulteBot difficulte;
	int score;
	int joker;
	
	/**
	 * Constructeur de l'objet Joueur avec attributs
	 * @param nom
	 * @param dateDeNaissance
	 */
	public Joueur(String nom, Date dateDeNaissance) {
		super();
		this.nom = nom;
		this.dateDeNaissance = dateDeNaissance;
		this.cartesJoueur = new ArrayList<Carte>();
		this.termine = false;
		this.bot = false;
		this.difficulte = null;
		this.score = 0;
		this.joker = 1;
	}
	
	/**
	 * Constructeur de l'objet Joueur avec attributs
	 * @param numéro
	 * @param difficulté
	 */
	public Joueur(int numero, DifficulteBot difficulte) {
		super();
		this.nom = "bot "+numero;
		this.dateDeNaissance = new Date();
		this.cartesJoueur = new ArrayList<Carte>();
		this.termine = false;
		this.bot = true;
		this.difficulte = difficulte;
		this.score = 0;
		this.joker = 1;
	}
	
	/**
	 * methodes retournant ou modifiant les attributs de la classe Carte
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public ArrayList<Carte> getCartesJoueur() {
		return cartesJoueur;
	}
	public void setCartesJoueur(ArrayList<Carte> cartesJoueur) {
		this.cartesJoueur = cartesJoueur;
	}
	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}
	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	public boolean isTermine() {
		return termine;
	}
	public void setTermine(boolean termine) {
		this.termine = termine;
	}
	public boolean isBot() {
		return bot;
	}
	public void setBot(boolean bot) {
		this.bot = bot;
	}
	public DifficulteBot getDifficulte() {
		return difficulte;
	}
	public void setDifficulte(DifficulteBot difficulte) {
		this.difficulte = difficulte;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getJoker() {
		return joker;
	}
	public void setJoker(int joker) {
		this.joker = joker;
	}

	/**
	 * retourne les informations sur le joueur
	 * @return String informations
	 */
	@Override
	public String toString() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return nom+" "+format.format(dateDeNaissance)+" score:"+score;
	}
	
	/**
	 * retourne une carte au hasard d'une main d'un joueur (utile pour bot)
	 * @return Carte informations
	 */
	public Carte choixCarteBot() {
		int rand = (int) (Math.random() * (this.cartesJoueur.size()));
		return this.cartesJoueur.get(rand);
	}
	
	
}
