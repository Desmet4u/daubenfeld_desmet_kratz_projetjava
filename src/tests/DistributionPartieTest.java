package tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import metier.*;
import enums.*;

class DistributionPartieTest {

	@Before
	public Jeu initialisationJoueurs(TypeCarte type, ThemeCarte theme, int nbJoueurs) throws IOException {
		Jeu jeu = new Jeu(new ArrayList<Joueur>());
		for (int i = 0; i < nbJoueurs; i++) {
			jeu.getJoueurs().add(new Joueur("test" + i, new Date()));
		}
		jeu.chargerCarte(type, theme);
		jeu.distribuerCarte();
		return jeu;
	}

	@Test
	void testDistributionDeuxJoueurs() throws IOException {
		Jeu jeu = initialisationJoueurs(TypeCarte.cardline, ThemeCarte.pib, 2);
		for(Joueur joueur : jeu.getJoueurs()) {
			Assertions.assertThat(joueur.getCartesJoueur().size()).isEqualTo(6);
		}
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getCartesPioche());
		Assertions.assertThat(jeu.getCartesPioche().size()+6*jeu.getJoueurs().size()+jeu.getCartesJeu().size()).isEqualTo(60);
		
		int nb = 2;
		for(int i = 0; i < nb; i++) {
			for(Carte c : jeu.getJoueurs().get(i).getCartesJoueur()) {
				Assertions.assertThat(c).isNotIn(jeu.getJoueurs().get((i+1)%nb).getCartesJoueur());
			}
		}
	}
	
	@Test
	void testDistributionQuatreJoueurs() throws IOException {
		Jeu jeu = initialisationJoueurs(TypeCarte.cardline, ThemeCarte.pollution, 4);
		for(Joueur joueur : jeu.getJoueurs()) {
			Assertions.assertThat(joueur.getCartesJoueur().size()).isEqualTo(5);
		}
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getCartesPioche());
		Assertions.assertThat(jeu.getCartesPioche().size()+5*jeu.getJoueurs().size()+jeu.getCartesJeu().size()).isEqualTo(60);
		
		int nb = 4;
		for(int i = 0; i < nb; i++) {
			for(Carte c : jeu.getJoueurs().get(i).getCartesJoueur()) {
				Assertions.assertThat(c).isNotIn(jeu.getJoueurs().get((i+1)%nb).getCartesJoueur());
			}
		}
	}
	
	@Test
	void testDistributionHuitJoueurs() throws IOException {
		Jeu jeu = initialisationJoueurs(TypeCarte.cardline, ThemeCarte.population, 8);
		for(Joueur joueur : jeu.getJoueurs()) {
			Assertions.assertThat(joueur.getCartesJoueur().size()).isEqualTo(4);
		}
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getCartesPioche());
		Assertions.assertThat(jeu.getCartesPioche().size()+4*jeu.getJoueurs().size()+jeu.getCartesJeu().size()).isEqualTo(60);
		
		int nb = 8;
		for(int i = 0; i < nb; i++) {
			for(Carte c : jeu.getJoueurs().get(i).getCartesJoueur()) {
				Assertions.assertThat(c).isNotIn(jeu.getJoueurs().get((i+1)%nb).getCartesJoueur());
			}
		}
	}
	
	@Test
	void testDistributionSeptJoueurs() throws IOException {
		Jeu jeu = initialisationJoueurs(TypeCarte.cardline, ThemeCarte.superficie, 7);
		for(Joueur joueur : jeu.getJoueurs()) {
			Assertions.assertThat(joueur.getCartesJoueur().size()).isEqualTo(4);
		}
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getCartesPioche());
		Assertions.assertThat(jeu.getCartesPioche().size()+4*jeu.getJoueurs().size()+jeu.getCartesJeu().size()).isEqualTo(60);
		
		int nb = 7;
		for(int i = 0; i < nb; i++) {
			for(Carte c : jeu.getJoueurs().get(i).getCartesJoueur()) {
				Assertions.assertThat(c).isNotIn(jeu.getJoueurs().get((i+1)%nb).getCartesJoueur());
			}
		}
	}
	
	@Test
	void testDistributionSixJoueurs() throws IOException {
		Jeu jeu = initialisationJoueurs(TypeCarte.timeline, null, 7);
		for(Joueur joueur : jeu.getJoueurs()) {
			Assertions.assertThat(joueur.getCartesJoueur().size()).isEqualTo(4);
		}
		Assertions.assertThat(jeu.getCartesJeu().get(0)).isNotIn(jeu.getCartesPioche());
		Assertions.assertThat(jeu.getCartesPioche().size()+4*jeu.getJoueurs().size()+jeu.getCartesJeu().size()).isEqualTo(60);
		
		int nb = 6;
		for(int i = 0; i < nb; i++) {
			for(Carte c : jeu.getJoueurs().get(i).getCartesJoueur()) {
				Assertions.assertThat(c).isNotIn(jeu.getJoueurs().get((i+1)%nb).getCartesJoueur());
			}
		}
	}
}
