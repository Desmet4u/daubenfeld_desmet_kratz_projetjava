package vues;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import enums.ThemeCarte;
import enums.TypeCarte;
import metier.Jeu;
import metier.Joueur;

public class InterfaceChoixJeu extends JFrame {

	private JPanel container = new JPanel();
	private JLabel choixJeu = new JLabel("À quel jeu souhaitez-vous jouer ?");
	private ArrayList<Joueur> joueurs;

	public InterfaceChoixJeu(ArrayList<Joueur> arraylistJoueurs) {
		joueurs = arraylistJoueurs;
		
		ImageIcon imageIconTimeline = new ImageIcon("data/image/timeline.png");
		Image imageTimeline = imageIconTimeline.getImage();
		Image newimgTimeline = imageTimeline.getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH);
		imageIconTimeline = new ImageIcon(newimgTimeline);
		JButton timelineButton = new JButton();
		timelineButton.setIcon(imageIconTimeline);
		timelineButton.setEnabled(true);
		timelineButton.setDisabledIcon(imageIconTimeline);
		
		ImageIcon imageIconCardline = new ImageIcon("data/image/cardline.png");
		Image imageCardline = imageIconCardline.getImage();
		Image newimgCardline = imageCardline.getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH);
		imageIconCardline = new ImageIcon(newimgCardline);
		JButton cardlineButton = new JButton();
		cardlineButton.setIcon(imageIconCardline);
		cardlineButton.setEnabled(true);
		cardlineButton.setDisabledIcon(imageIconCardline);
		
		setLayout(new BorderLayout());
		timelineButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				//InterfaceJeu jeu = new InterfaceJeu(joueurs, TypeCarte.timeline, null);
				Jeu test = new Jeu();
				test.getJoueurs().addAll(joueurs);
				test.setTypeJeu(TypeCarte.timeline);
				test.setThemeJeu(null);
				try {
					VueJeu jeu = new VueJeu(test);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		cardlineButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JPanel cardLinePanel = new JPanel();
				JLabel cardLineLabel = new JLabel("Avec quel thème souhaitez-vous jouer ?");
				JButton jouer = new JButton("Jouer");
				cardLinePanel.setLayout(new GridLayout(6,1));
				
				CheckboxGroup cb = new CheckboxGroup();
				Checkbox superficie = new Checkbox("Superficie", cb, false);
				Checkbox pop = new Checkbox("Population", cb, false);
				Checkbox pib = new Checkbox("PIB", cb, false);
				Checkbox pollution = new Checkbox("Pollution", cb, false);

				jouer.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						if (superficie.getState() || pop.getState() || pib.getState() || pollution.getState()) {
							Jeu test = new Jeu();
							test.getJoueurs().addAll(joueurs);
							test.setTypeJeu(TypeCarte.cardline);

							if (superficie.getState())
								test.setThemeJeu(ThemeCarte.superficie);
							else if (pop.getState())
								test.setThemeJeu(ThemeCarte.population);
							else if (pib.getState())
								test.setThemeJeu(ThemeCarte.pib);
							else
								test.setThemeJeu(ThemeCarte.pollution);
							
							try {
								dispose();
								VueJeu jeu = new VueJeu(test);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}

						}
					}
				});
				
				cardLinePanel.add(cardLineLabel);
				cardLinePanel.add(superficie);
				cardLinePanel.add(pop);
				cardLinePanel.add(pib);
				cardLinePanel.add(pollution);
				cardLinePanel.add(jouer);

				add(cardLinePanel,BorderLayout.CENTER);
				pack();
			}
		});
		
		
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(getParent(), "Êtes-vous sur de vouloir quitter le jeu ?",
						"Fermeture du jeu ?", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});
		
		container.add(choixJeu);
		container.add(timelineButton);
		container.add(cardlineButton);
		add(container,BorderLayout.NORTH);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
}
